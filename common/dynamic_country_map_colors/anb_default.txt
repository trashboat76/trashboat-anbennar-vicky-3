﻿#
# Note: the triggers are checked at certain key points in the code, not all the time
# color_key = {
#    color = "black"      # the map color
#    possible = { ... }   # the trigger to enable this color
# }
#

# Example
#dynamic_map_color_swedish_junta = {
#    color = "black"
#	
#	possible = {
#		exists = c:SWE # you /must/ check that a tag exists before doing anything else
#		this = c:SWE
#
#        exists = ig:ig_armed_forces
#        ig:ig_armed_forces = {
#            is_in_government = yes
#        }
#	}
#}

chippengard_yellow = {
	color = "chippengard_yellow"

	possible = {
		exists = c:B30
		THIS = c:B30
		
		NOT = {
			country_has_state_religion = rel:ynn_river_worship
			top_overlord = {
				country_has_state_religion = rel:ynn_river_worship
			}
		}
	}
}

corinsfield_orange = {
	color = "corinsfield_orange"

	possible = {
		exists = c:B33
		THIS = c:B33
		
		NOT = {
			country_has_state_religion = rel:ynn_river_worship
			top_overlord = {
				country_has_state_religion = rel:ynn_river_worship
			}
		}
	}
}
 # Dostanor / Vivin Empire
dostanor_red = {
	color = "dostanor_red"

	possible = {
		THIS ?= c:A02
		coa_communist_trigger = yes
	}
}
vivin_blue = {
	color = "vivin_blue"

	possible = {
		THIS ?= c:A02
		coa_monarchy_trigger = yes
	}
}

tipney_red = {
	color = "tipney_red"

	possible = {
		exists = c:B32
		THIS = c:B32
		
		NOT = {
			country_has_state_religion = rel:ynn_river_worship
			top_overlord = {
				country_has_state_religion = rel:ynn_river_worship
			}
		}
	}
}