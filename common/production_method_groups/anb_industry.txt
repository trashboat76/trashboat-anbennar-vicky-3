﻿pmg_reagents_building_magical_reagents_workshop = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_adventurer_acquisition_building_magical_reagents_workshop
		pm_porcelain_reagents_building_magical_reagents_workshop
        pm_relic_reagents_building_magical_reagents_workshop
	}
}

pmg_doodads_building_magical_reagents_workshop = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_no_doodads_building_magical_reagents_workshop
        pm_basic_doodads_building_magical_reagents_workshop
	}
}

pmg_relics_building_magical_reagents_workshop = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_standard_production_building_magical_reagents_workshop
        pm_relic_processing_building_magical_reagents_workshop
	}
}

pmg_ownership_capital_building_magical_reagents_workshop = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_ownership.dds"
	production_methods = {
		pm_merchant_guilds_building_magical_reagents_workshop
		pm_privately_owned_building_magical_reagents_workshop
		pm_publicly_traded_building_magical_reagents_workshop
		pm_government_run_building_magical_reagents_workshop
		pm_worker_cooperative_building_magical_reagents_workshop
	}
}

pmg_base_building_skyship_yards = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_sky_galleons_building_skyship_yards
        pm_steel_skyships_building_skyship_yards
		pm_arc_welded_skyships_building_skyship_yards
	}
}

pmg_military_base_building_skyship_yards = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_civilian_shipbuilding_building_skyship_yards
        pm_military_shipbuilding_skyship_yards
		pm_extensive_military_shipbuilding_building_skyship_yards
	}
}

pmg_ownership_capital_building_skyship_yards = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_ownership.dds"
	production_methods = {
		pm_merchant_guilds_building_skyship_yards
		pm_privately_owned_building_skyship_yards
		pm_publicly_traded_building_skyship_yards
		pm_government_run_building_skyship_yards
		pm_worker_cooperative_building_skyship_yards
	}
}

pmg_base_building_damesoil_refinery = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_basic_liquefaction_building_damesoil_refinery
		pm_reagent_liquefaction_building_damesoil_refinery
		pm_doodad_liquefaction_building_damesoil_refinery
	}
}

pmg_storage_building_damesoil_refinery = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_no_storage_building_damesoil_refinery
		pm_porcelain_storage_building_damesoil_refinery
		pm_steel_storage_building_damesoil_refinery
	}
}

pmg_transmutation_building_damesoil_refinery = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_no_transmutation_building_damesoil_refinery
		pm_coal_transmutation_building_damesoil_refinery
	}
}

pmg_ownership_capital_building_damesoil_refinery = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_ownership.dds"
	production_methods = {
		pm_merchant_guilds_building_damesoil_refinery
		pm_privately_owned_building_damesoil_refinery
		pm_publicly_traded_building_damesoil_refinery
		pm_government_run_building_damesoil_refinery
		pm_worker_cooperative_building_damesoil_refinery
	}
}

pmg_collection_procedure_building_blueblood_plants = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_syringes_building_blueblood_plants
		pm_arcane_leeches_building_blueblood_plants
		pm_full_transfusion_building_blueblood_plants
		pm_mechanical_extractors_building_blueblood_plants
		pm_blood_farms_building_blueblood_plants
	}
}

pmg_patient_acquisition_building_blueblood_plants = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_donation_centres_building_blueblood_plants
		pm_penal_patients_building_blueblood_plants
		pm_unwilling_subjects_building_blueblood_plants
		pm_industrial_scale_acquisition_building_blueblood_plants
	}
}

pmg_refrigeration_building_blueblood_plants = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_unrefrigerated_building_blueblood_plants
		pm_frost_magic_refrigeration_building_blueblood_plants
		pm_refrigerated_storage_building_blueblood_plants
		pm_chemical_refrigeration_building_blueblood_plants
	}
}

pmg_ownership_capital_building_blueblood_plants = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_ownership.dds"
	production_methods = {
		pm_merchant_guilds_building_blueblood_plants
		pm_privately_owned_building_blueblood_plants
		pm_publicly_traded_building_blueblood_plants
		pm_government_run_building_blueblood_plants
		pm_worker_cooperative_building_blueblood_plants
	}
}

pmg_power_supply_building_doodad_manufacturies = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_crystal_socketry_building_doodad_manufacturies
		pm_damestear_alloying_building_doodad_manufacturies
		pm_damesoil_tubes_building_doodad_manufacturies
		pm_damestear_core_building_doodad_manufacturies
	}
}

pmg_casing_building_doodad_manufacturies = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_artisan_artificery_building_doodad_manufacturies
		pm_relics_casings_building_doodad_manufacturies
		pm_tearite_casings_building_doodad_manufacturies
		pm_porcelain_stabilized_tearite_casings_building_doodad_manufacturies
		pm_mimic_precursor_steel_casings_building_doodad_manufacturies
	}
}

pmg_wiring_building_doodad_manufacturies = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_copper_wires_building_doodad_manufacturies
		pm_arcane_copper_wires_building_doodad_manufacturies
		pm_dagrite_autowires_building_doodad_manufacturies
	}
}

pmg_automation_building_doodad_manufacturies = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_automation_disabled_building_doodad_manufacturies
		pm_automata_laborers_doodad_manufacturies
		pm_rotary_valve_engine_building_doodad_manufacturies
		pm_automata_machinists_doodad_manufacturies
		pm_assembly_lines_building_doodad_manufacturies
	}
}

pmg_ownership_capital_building_doodad_manufacturies = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_ownership.dds"
	production_methods = {
		pm_merchant_guilds_building_doodad_manufacturies
		pm_privately_owned_building_doodad_manufacturies
		pm_publicly_traded_building_doodad_manufacturies
		pm_government_run_building_doodad_manufacturies
		pm_worker_cooperative_building_doodad_manufacturies
	}
}

pmg_processor_building_automatories = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_mind_rune_logic
		pm_network_gadgetry
		pm_early_processors
		pm_ensouled_artifice
	}
}

pmg_power_source_building_automatories = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_sparkdrive_engine_automatories
		pm_bound_elemental
		pm_internal_combustion_engine_automatories
		pm_damestear_reactor_automatories
	}
}

pmg_frame_building_automatories = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_iron_golem_frame
		pm_early_mechanim_frame
		pm_advanced_mechanim_frame
	}
}

pmg_automation_building_automatories = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_no_automation
		pm_self_repairing_automata_automatories
		pm_self_building_automata_automatories
	}
}

pmg_ownership_capital_building_automatories = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_ownership.dds"
	production_methods = {
		pm_merchant_guilds_building_automatories
		pm_privately_owned_building_automatories
		pm_publicly_traded_building_automatories
		pm_government_run_building_automatories
		pm_worker_cooperative_building_automatories
	}
}