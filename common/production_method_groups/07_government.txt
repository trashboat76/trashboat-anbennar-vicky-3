﻿pmg_base_building_port = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	ai_selection = most_productive

	production_methods = {
		pm_anchorage
		pm_basic_port
		pm_industrial_port
		pm_modern_port
		pm_self_regulating_megaport # Anbennar
	}
}

pmg_base_building_government_administration = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	ai_selection = most_productive

	production_methods = {
		pm_simple_organization
		pm_horizontal_drawer_cabinets
		pm_vertical_filing_cabinets
		pm_sending_stone_communications # Anbennar
		pm_deputized_familiars #Anbennar
		pm_switch_boards
		pm_t_wave_transmissions # Anbennar
	}
}

pmg_enhancements_government_administration = { # Anbennar
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_no_enhancements # Anbennar
		pm_divined_tax_returns # Anbennar
		pm_automated_translators # Anbennar
	}
}

pmg_automation_government_administration = { # Anbennar	
texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	ai_selection = most_profitable
	production_methods = {
		pm_no_automation # Anbennar
		pm_self_cleaning_parchment_government # Anbennar
		pm_arithmaton_calculators_government # Anbennar
	}
}

pmg_government_administration_bureaucrat_professionalism = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_ownership.dds"
	ai_selection = most_productive

	production_methods = {
		pm_hereditary_bureaucrats
		pm_professional_bureaucrats
	}
}

pmg_government_administration_religious_administration = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_ownership.dds"
	ai_selection = most_productive

	production_methods = {
		pm_religious_bureaucrats
		pm_magical_bureaucrats #Anbennar
		pm_secular_bureaucrats
	}
}

pmg_base_building_university = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	ai_selection = most_productive

	production_methods = {
		pm_scholastic_education
		pm_philosophy_department
		pm_analytical_philosophy_department
	}
}

pmg_magical_studies_university = { # Anbennar
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_no_magical_studies # Anbennar
		pm_spell_components_studies # Anbennar
		pm_relics_reverse_enginering # Anbennar 
		pm_precursor_immersion_studies # Anbennar
	}
}

pmg_automation_university = { # Anbennar
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_no_automation # Anbennar
		pm_self_cleaning_parchment_universities # Anbennar
		pm_arithmaton_calculators_universities # Anbennar
	}
}

pmg_university_academia = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_ownership.dds"
	ai_selection = most_productive

	production_methods = {
		pm_religious_academia
		pm_magical_academia #Anbennar
		pm_secular_academia
	}
}

pmg_base_building_skyscraper = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	ai_selection = most_productive

	production_methods = {
		pm_skyscraper_bureaucratic_nexus
		pm_skyscraper_trade_nexus
	}
}

pmg_airship_mooring_post = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_refining.dds"
	ai_selection = most_productive

	production_methods = {
		pm_no_airships
		pm_airship_mooring_post
	}
}
