﻿#Anbennar Law Groups

lawgroup_racial_tolerance = {
	law_group_category = power_structure
	
	progressive_movement_chance = 4	#copy of citizenship btw
	regressive_movement_chance = 0.1
}

lawgroup_magic_and_artifice = {
	law_group_category = economy
}

# lawgroup_right_to_adventure = {
	# law_group_category = human_rights
# }

lawgroup_mage_ethics = {
	law_group_category = power_structure
}

lawgroup_artificer_ethics = {
	law_group_category = power_structure
}
