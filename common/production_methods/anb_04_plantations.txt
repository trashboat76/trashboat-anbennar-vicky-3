﻿## Coffee Plantation
conjurative_irrigation_coffee_plantation = {
	texture = "gfx/interface/icons/production_method_icons/plantation_production.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_artificery_doodads_add = 5
			
			# output goods
			goods_output_coffee_add = 30
		}

		level_scaled = {
			building_employment_laborers_add = 3500
			building_employment_machinists_add = 250
			building_employment_farmers_add = 1000
			building_employment_clergymen_add = 100
		}
	}
}

pm_mass_cast_plant_growth_coffee_plantation = {
	texture = "gfx/interface/icons/production_method_icons/soil_enriching_farming.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods													
			goods_input_reagents_add = 5				
			
			# output goods													
			goods_output_coffee_add = 10
		}

		level_scaled = {
			building_employment_mages_add = 250
		}
	}
}

pm_spirit_imbuement_coffee_plantation = {
	texture = "gfx/interface/icons/production_method_icons/soil_enriching_farming.dds"

	unlocking_technologies = {
		intensive_agriculture
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods													
			goods_input_spirit_energy_add = 10
			goods_input_fertilizer_add = 5		
			
			# output goods													
			goods_output_coffee_add = 18
		}

		level_scaled = {
			# earnings														
			building_employment_laborers_add = -500
			building_employment_machinists_add = 500
		}
	}
}

pm_growth_serum_coffee_plantation = {
	texture = "gfx/interface/icons/production_method_icons/fertilization.dds"
	
	unlocking_technologies = {
		improved_fertilizer
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods													
			goods_input_spirit_energy_add = 20
			goods_input_fertilizer_add = 5
			goods_input_artificery_doodads_add = 5		
			
			# output goods													
			goods_output_coffee_add = 36
		}

		level_scaled = {
			# earnings														
			building_employment_laborers_add = -750
			building_employment_machinists_add = 500
			building_employment_engineers_add = 250
		}
	}
}

## Cotton Plantation
conjurative_irrigation_cotton_plantation = {
	texture = "gfx/interface/icons/production_method_icons/plantation_production.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_artificery_doodads_add = 5
			
			# output goods
			goods_output_fabric_add = 70
		}

		level_scaled = {
			building_employment_laborers_add = 3500
			building_employment_machinists_add = 250
			building_employment_farmers_add = 1000
			building_employment_clergymen_add = 100
		}
	}
}

pm_mass_cast_plant_growth_cotton_plantation = {
	texture = "gfx/interface/icons/production_method_icons/soil_enriching_farming.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods													
			goods_input_reagents_add = 5				
			
			# output goods													
			goods_output_fabric_add = 20
		}

		level_scaled = {
			building_employment_mages_add = 250
		}
	}
}

pm_spirit_imbuement_cotton_plantation = {
	texture = "gfx/interface/icons/production_method_icons/soil_enriching_farming.dds"

	unlocking_technologies = {
		intensive_agriculture
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods													
			goods_input_spirit_energy_add = 10
			goods_input_fertilizer_add = 5		
			
			# output goods													
			goods_output_fabric_add = 40
		}

		level_scaled = {
			# earnings														
			building_employment_laborers_add = -500
			building_employment_machinists_add = 500
		}
	}
}

pm_growth_serum_cotton_plantation = {
	texture = "gfx/interface/icons/production_method_icons/fertilization.dds"
	
	unlocking_technologies = {
		improved_fertilizer
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods													
			goods_input_spirit_energy_add = 20
			goods_input_fertilizer_add = 5
			goods_input_artificery_doodads_add = 5		
			
			# output goods													
			goods_output_fabric_add = 80
		}

		level_scaled = {
			# earnings														
			building_employment_laborers_add = -750
			building_employment_machinists_add = 500
			building_employment_engineers_add = 250
		}
	}
}

## Dye Plantation
conjurative_irrigation_dye_plantation = {
	texture = "gfx/interface/icons/production_method_icons/plantation_production.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_artificery_doodads_add = 5
			
			# output goods
			goods_output_dye_add = 37
		}

		level_scaled = {
			building_employment_laborers_add = 3500
			building_employment_machinists_add = 250
			building_employment_farmers_add = 1000
			building_employment_clergymen_add = 100
		}
	}
}

pm_mass_cast_plant_growth_dye_plantation = {
	texture = "gfx/interface/icons/production_method_icons/soil_enriching_farming.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods													
			goods_input_reagents_add = 5				
			
			# output goods													
			goods_output_dye_add = 12
		}

		level_scaled = {
			building_employment_mages_add = 250
		}
	}
}

pm_spirit_imbuement_dye_plantation = {
	texture = "gfx/interface/icons/production_method_icons/soil_enriching_farming.dds"

	unlocking_technologies = {
		intensive_agriculture
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods													
			goods_input_spirit_energy_add = 10
			goods_input_fertilizer_add = 5		
			
			# output goods													
			goods_output_dye_add = 22
		}

		level_scaled = {
			# earnings														
			building_employment_laborers_add = -500
			building_employment_machinists_add = 500
		}
	}
}

pm_growth_serum_dye_plantation = {
	texture = "gfx/interface/icons/production_method_icons/fertilization.dds"
	
	unlocking_technologies = {
		improved_fertilizer
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods													
			goods_input_spirit_energy_add = 20
			goods_input_fertilizer_add = 5
			goods_input_artificery_doodads_add = 5		
			
			# output goods													
			goods_output_dye_add = 44
		}

		level_scaled = {
			# earnings														
			building_employment_laborers_add = -750
			building_employment_machinists_add = 500
			building_employment_engineers_add = 250
		}
	}
}

## Opium Plantation
conjurative_irrigation_opium_plantation = {
	texture = "gfx/interface/icons/production_method_icons/plantation_production.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_artificery_doodads_add = 5
			
			# output goods
			goods_output_opium_add = 37
		}

		level_scaled = {
			building_employment_laborers_add = 3500
			building_employment_machinists_add = 250
			building_employment_farmers_add = 1000
			building_employment_clergymen_add = 100
		}
	}
}

pm_mass_cast_plant_growth_opium_plantation = {
	texture = "gfx/interface/icons/production_method_icons/soil_enriching_farming.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods													
			goods_input_reagents_add = 5				
			
			# output goods													
			goods_output_opium_add = 12
		}

		level_scaled = {
			building_employment_mages_add = 250
		}
	}
}

pm_spirit_imbuement_opium_plantation = {
	texture = "gfx/interface/icons/production_method_icons/soil_enriching_farming.dds"

	unlocking_technologies = {
		intensive_agriculture
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods													
			goods_input_spirit_energy_add = 10
			goods_input_fertilizer_add = 5		
			
			# output goods													
			goods_output_opium_add = 22
		}

		level_scaled = {
			# earnings														
			building_employment_laborers_add = -500
			building_employment_machinists_add = 500
		}
	}
}

pm_growth_serum_opium_plantation = {
	texture = "gfx/interface/icons/production_method_icons/fertilization.dds"
	
	unlocking_technologies = {
		improved_fertilizer
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods													
			goods_input_spirit_energy_add = 20
			goods_input_fertilizer_add = 5
			goods_input_artificery_doodads_add = 5		
			
			# output goods													
			goods_output_opium_add = 44
		}

		level_scaled = {
			# earnings														
			building_employment_laborers_add = -750
			building_employment_machinists_add = 500
			building_employment_engineers_add = 250
		}
	}
}

## Tea Plantation
conjurative_irrigation_tea_plantation = {
	texture = "gfx/interface/icons/production_method_icons/plantation_production.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_artificery_doodads_add = 5
			
			# output goods
			goods_output_tea_add = 30
		}

		level_scaled = {
			building_employment_laborers_add = 3500
			building_employment_machinists_add = 250
			building_employment_farmers_add = 1000
			building_employment_clergymen_add = 100
		}
	}
}

pm_mass_cast_plant_growth_tea_plantation = {
	texture = "gfx/interface/icons/production_method_icons/soil_enriching_farming.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods													
			goods_input_reagents_add = 5				
			
			# output goods													
			goods_output_tea_add = 10
		}

		level_scaled = {
			building_employment_mages_add = 250
		}
	}
}

pm_spirit_imbuement_tea_plantation = {
	texture = "gfx/interface/icons/production_method_icons/soil_enriching_farming.dds"

	unlocking_technologies = {
		intensive_agriculture
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods													
			goods_input_spirit_energy_add = 10
			goods_input_fertilizer_add = 5		
			
			# output goods													
			goods_output_tea_add = 18
		}

		level_scaled = {
			# earnings														
			building_employment_laborers_add = -500
			building_employment_machinists_add = 500
		}
	}
}

pm_growth_serum_tea_plantation = {
	texture = "gfx/interface/icons/production_method_icons/fertilization.dds"
	
	unlocking_technologies = {
		improved_fertilizer
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods													
			goods_input_spirit_energy_add = 20
			goods_input_fertilizer_add = 5
			goods_input_artificery_doodads_add = 5		
			
			# output goods													
			goods_output_tea_add = 36
		}

		level_scaled = {
			# earnings														
			building_employment_laborers_add = -750
			building_employment_machinists_add = 500
			building_employment_engineers_add = 250
		}
	}
}

## Tobacco Plantation
conjurative_irrigation_tobacco_plantation = {
	texture = "gfx/interface/icons/production_method_icons/plantation_production.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_artificery_doodads_add = 5
			
			# output goods
			goods_output_tobacco_add = 37
		}

		level_scaled = {
			building_employment_laborers_add = 3500
			building_employment_machinists_add = 250
			building_employment_farmers_add = 1000
			building_employment_clergymen_add = 100
		}
	}
}

pm_mass_cast_plant_growth_tobacco_plantation = {
	texture = "gfx/interface/icons/production_method_icons/soil_enriching_farming.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods													
			goods_input_reagents_add = 5				
			
			# output goods													
			goods_output_tobacco_add = 12
		}

		level_scaled = {
			building_employment_mages_add = 250
		}
	}
}

pm_spirit_imbuement_tobacco_plantation = {
	texture = "gfx/interface/icons/production_method_icons/soil_enriching_farming.dds"

	unlocking_technologies = {
		intensive_agriculture
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods													
			goods_input_spirit_energy_add = 10
			goods_input_fertilizer_add = 5		
			
			# output goods													
			goods_output_tobacco_add = 22
		}

		level_scaled = {
			# earnings														
			building_employment_laborers_add = -500
			building_employment_machinists_add = 500
		}
	}
}

pm_growth_serum_tobacco_plantation = {
	texture = "gfx/interface/icons/production_method_icons/fertilization.dds"
	
	unlocking_technologies = {
		improved_fertilizer
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods													
			goods_input_spirit_energy_add = 20
			goods_input_fertilizer_add = 5
			goods_input_artificery_doodads_add = 5		
			
			# output goods													
			goods_output_tobacco_add = 44
		}

		level_scaled = {
			# earnings														
			building_employment_laborers_add = -750
			building_employment_machinists_add = 500
			building_employment_engineers_add = 250
		}
	}
}

## Sugar Plantation
conjurative_irrigation_sugar_plantation = {
	texture = "gfx/interface/icons/production_method_icons/plantation_production.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_artificery_doodads_add = 5
			
			# output goods
			goods_output_sugar_add = 45
		}

		level_scaled = {
			building_employment_laborers_add = 3500
			building_employment_machinists_add = 250
			building_employment_farmers_add = 1000
			building_employment_clergymen_add = 100
		}
	}
}

pm_mass_cast_plant_growth_sugar_plantation = {
	texture = "gfx/interface/icons/production_method_icons/soil_enriching_farming.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods													
			goods_input_reagents_add = 5				
			
			# output goods													
			goods_output_sugar_add = 15
		}

		level_scaled = {
			building_employment_mages_add = 250
		}
	}
}

pm_spirit_imbuement_sugar_plantation = {
	texture = "gfx/interface/icons/production_method_icons/soil_enriching_farming.dds"

	unlocking_technologies = {
		intensive_agriculture
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods													
			goods_input_spirit_energy_add = 10
			goods_input_fertilizer_add = 5		
			
			# output goods													
			goods_output_sugar_add = 30
		}

		level_scaled = {
			# earnings														
			building_employment_laborers_add = -500
			building_employment_machinists_add = 500
		}
	}
}

pm_growth_serum_sugar_plantation = {
	texture = "gfx/interface/icons/production_method_icons/fertilization.dds"
	
	unlocking_technologies = {
		improved_fertilizer
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods													
			goods_input_spirit_energy_add = 20
			goods_input_fertilizer_add = 5
			goods_input_artificery_doodads_add = 5		
			
			# output goods													
			goods_output_sugar_add = 60
		}

		level_scaled = {
			# earnings														
			building_employment_laborers_add = -750
			building_employment_machinists_add = 500
			building_employment_engineers_add = 250
		}
	}
}


## Fruit Plantation
conjurative_irrigation_fruit_plantation = {
	texture = "gfx/interface/icons/production_method_icons/plantation_production.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_artificery_doodads_add = 5
			
			# output goods
			goods_output_fruit_add = 45
		}

		level_scaled = {
			building_employment_laborers_add = 3500
			building_employment_machinists_add = 250
			building_employment_farmers_add = 1000
			building_employment_clergymen_add = 100
		}
	}
}

pm_mass_cast_plant_growth_fruit_plantation = {
	texture = "gfx/interface/icons/production_method_icons/soil_enriching_farming.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods													
			goods_input_reagents_add = 5				
			
			# output goods													
			goods_output_fruit_add = 15
		}

		level_scaled = {
			building_employment_mages_add = 250
		}
	}
}

pm_spirit_imbuement_fruit_plantation = {
	texture = "gfx/interface/icons/production_method_icons/soil_enriching_farming.dds"

	unlocking_technologies = {
		intensive_agriculture
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods													
			goods_input_spirit_energy_add = 10
			goods_input_fertilizer_add = 5		
			
			# output goods													
			goods_output_fruit_add = 30
		}

		level_scaled = {
			# earnings														
			building_employment_laborers_add = -500
			building_employment_machinists_add = 500
		}
	}
}

pm_growth_serum_fruit_plantation = {
	texture = "gfx/interface/icons/production_method_icons/fertilization.dds"
	
	unlocking_technologies = {
		improved_fertilizer
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods													
			goods_input_spirit_energy_add = 20
			goods_input_fertilizer_add = 5
			goods_input_artificery_doodads_add = 5		
			
			# output goods													
			goods_output_fruit_add = 60
		}

		level_scaled = {
			# earnings														
			building_employment_laborers_add = -750
			building_employment_machinists_add = 500
			building_employment_engineers_add = 250
		}
	}
}

## Silk Plantation
conjurative_irrigation_silk_plantation = {
	texture = "gfx/interface/icons/production_method_icons/plantation_production.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_artificery_doodads_add = 5
			
			# output goods
			goods_output_silk_add = 30
		}

		level_scaled = {
			building_employment_laborers_add = 3500
			building_employment_machinists_add = 250
			building_employment_farmers_add = 1000
			building_employment_clergymen_add = 100
		}
	}
}

pm_mass_cast_plant_growth_silk_plantation = {
	texture = "gfx/interface/icons/production_method_icons/soil_enriching_farming.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods													
			goods_input_reagents_add = 5				
			
			# output goods													
			goods_output_silk_add = 10
		}

		level_scaled = {
			building_employment_mages_add = 250
		}
	}
}

pm_spirit_imbuement_silk_plantation = {
	texture = "gfx/interface/icons/production_method_icons/soil_enriching_farming.dds"

	unlocking_technologies = {
		intensive_agriculture
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods													
			goods_input_spirit_energy_add = 10
			goods_input_fertilizer_add = 5		
			
			# output goods													
			goods_output_silk_add = 18
		}

		level_scaled = {
			# earnings														
			building_employment_laborers_add = -500
			building_employment_machinists_add = 500
		}
	}
}

pm_growth_serum_silk_plantation = {
	texture = "gfx/interface/icons/production_method_icons/fertilization.dds"
	
	unlocking_technologies = {
		improved_fertilizer
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods													
			goods_input_spirit_energy_add = 20
			goods_input_fertilizer_add = 5
			goods_input_artificery_doodads_add = 5		
			
			# output goods													
			goods_output_silk_add = 36
		}

		level_scaled = {
			# earnings														
			building_employment_laborers_add = -750
			building_employment_machinists_add = 500
			building_employment_engineers_add = 250
		}
	}
}

## Wine Plantation
conjurative_irrigation_wine_plantation = {
	texture = "gfx/interface/icons/production_method_icons/plantation_production.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_artificery_doodads_add = 5
			
			# output goods
			goods_output_wine_add = 30
		}

		level_scaled = {
			building_employment_laborers_add = 3500
			building_employment_machinists_add = 250
			building_employment_farmers_add = 1000
			building_employment_clergymen_add = 100
		}
	}
}

pm_spirit_imbuement_wine_plantation = {
	texture = "gfx/interface/icons/production_method_icons/soil_enriching_farming.dds"

	unlocking_technologies = {
		intensive_agriculture
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods													
			goods_input_spirit_energy_add = 10
			goods_input_fertilizer_add = 5		
			
			# output goods													
			goods_output_wine_add = 18
		}

		level_scaled = {
			# earnings														
			building_employment_laborers_add = -500
			building_employment_machinists_add = 500
		}
	}
}

pm_growth_serum_wine_plantation = {
	texture = "gfx/interface/icons/production_method_icons/fertilization.dds"
	
	unlocking_technologies = {
		improved_fertilizer
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods													
			goods_input_spirit_energy_add = 20
			goods_input_fertilizer_add = 5
			goods_input_artificery_doodads_add = 5		
			
			# output goods													
			goods_output_wine_add = 36
		}

		level_scaled = {
			# earnings														
			building_employment_laborers_add = -750
			building_employment_machinists_add = 500
			building_employment_engineers_add = 250
		}
	}
}