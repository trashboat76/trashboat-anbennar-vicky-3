﻿goods_output_damestear_add = {
	good = yes
	percent = no
}

goods_input_damestear_add = {
	good = no
	percent = no
}

goods_output_artificery_doodads_add = {
	good = yes
	percent = no
}

goods_input_artificery_doodads_add = {
	good = no
	percent = no
}

goods_output_blueblood_add = {
	good = yes
	percent = no
}

goods_input_blueblood_add = {
	good = no
	percent = no
}

goods_output_relics_add = {
	good = yes
	percent = no
}

goods_input_relics_add = {
	good = no
	percent = no
}

goods_output_reagents_add = {
	good = yes
	percent = no
}

goods_input_reagents_add = {
	good = no
	percent = no
}

goods_output_perfect_metal_add = {
	good = yes
	percent = no
}

goods_input_perfect_metal_add = {
	good = no
	percent = no
}

goods_output_automata_add = {
	good = yes
	percent = no
}

goods_input_automata_add = {
	good = no
	percent = no
}

goods_output_spirit_energy_add = {
	good = yes
	percent = no
}

goods_input_spirit_energy_add = {
	good = no
	percent = no
}

goods_output_commercial_skyships_add = {
	good = yes
	percent = no
}

goods_input_commercial_skyships_add = {
	good = no
	percent = no
}

goods_output_military_skyships_add = {
	good = yes
	percent = no
}

goods_input_military_skyships_add = {
	good = no
	percent = no
}

goods_input_porcelain_add = { #Not in vanilla since it is only a consumer good there
	good = no
	percent = no
}

goods_output_groceries_mult = {
	good = yes
	percent = yes
}

goods_output_porcelain_mult = {
	good = yes
	percent = yes
}

goods_output_fish_mult = {
	good = yes
	percent = yes
}

building_damestear_mine_throughput_add = {
	good = yes
	percent = yes
}

building_damestear_fields_throughput_add = {
	good = yes
	percent = yes
}

building_perfect_metal_mine_throughput_add = {
	good = yes
	percent = yes
}

building_mage_academy_throughput_add = {
	good = yes
	percent = yes
}

building_group_bg_cave_coral_throughput_add = {
	good = yes
	percent = yes
	num_decimals = 0
}

building_doodad_manufacturies_throughput_add = {
	good = yes
	percent = yes
}

building_automatories_throughput_add = {
	good = yes
	percent = yes
}