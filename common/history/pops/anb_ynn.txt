﻿POPS = {
	s:STATE_NIZVELS = {
		region_state:B29 = {
			create_pop = {
				culture = sarda
				size = 360000
			}
			create_pop = {
				culture = steadsman
				size = 20000
			}
			create_pop = {
				culture = ynngarder_half_orc
				size = 20000
			}
		}
	}
	s:STATE_HRADAPOLERE = {
		region_state:B29 = {
			create_pop = {
				culture = sarda
				size = 637500
			}
			create_pop = {
				culture = veykodan
				size = 37500
			}
			create_pop = {
				culture = ynngarder_half_orc
				size = 37500
			}
			create_pop = {
				culture = new_havoric
				size = 37500
			}
		}
	}
	s:STATE_VYCHVELS = {
		region_state:B29 = {
			create_pop = {
				culture = sarda
				size = 292500
			}
			create_pop = {
				culture = veykodan
				size = 90000
			}
			create_pop = {
				culture = pipefoot_halfling
				size = 45000
			}
			create_pop = {
				culture = new_havoric
				size = 22500
			}
		}
	}
	s:STATE_YNNPADH = {
		region_state:B29 = {
			create_pop = {
				culture = sarda
				size = 137500
			}
			create_pop = {
				culture = veykodan
				size = 75000
			}
			create_pop = {
				culture = boek
				size = 12500
			}
			create_pop = {
				culture = pipefoot_halfling
				size = 12500
			}
			create_pop = {
				culture = new_havoric
				size = 12500
			}
		}
	}
	s:STATE_YRISRAD = {
		region_state:B29 = {
			create_pop = {
				culture = sarda
				size = 735000
			}
			create_pop = {
				culture = veykodan
				size = 105000
			}
			create_pop = {
				culture = corinsfielder
				size = 52500
			}
			create_pop = {
				culture = pipefoot_halfling
				size = 52500
			}
			create_pop = {
				culture = ynngarder_half_orc
				size = 52500
			}
			create_pop = {
				culture = steel_dwarf
				size = 52500
			}
		}
	}
	s:STATE_VIZANIRZAG = {
		region_state:B31 = {
			create_pop = {
				culture = veykodan
				size = 187500
			}
			create_pop = {
				culture = sarda
				size = 37500
			}
			create_pop = {
				culture = pipefoot_halfling
				size = 25000
			}
		}
	}
	s:STATE_LETHIR = {
		region_state:B31 = {
			create_pop = {
				culture = new_havoric
				size = 157500
			}
			create_pop = {
				culture = veykodan
				size = 33750
			}
			create_pop = {
				culture = sarda
				size = 22500
			}
			create_pop = {
				culture = pipefoot_halfling
				size = 11250
			}
		}
	}
	s:STATE_CORINSFIELD = {
		region_state:B33 = {
			create_pop = {
				culture = corinsfielder
				size = 322000
			}
			create_pop = {
				culture = sarda
				size = 10500
			}
			create_pop = {
				culture = veykodan
				size = 17500
			}
		}
	}
	s:STATE_WEST_TIPNEY = {
		region_state:B32 = { #Majority Halfling, followed by Ruinborn, human, horc, dwarf
			create_pop = {
				culture = pipefoot_halfling
				size = 247500
			}
			create_pop = {
				culture = veykodan
				size = 90000
			}
			create_pop = {
				culture = sarda
				size = 45000
			}
			create_pop = {
				culture = new_havoric
				size = 67500
			}
		}
	}
	s:STATE_OSINDAIN = {
		region_state:B35 = {
			create_pop = {
				culture = new_havoric
				size = 148750
			}
			create_pop = {
				culture = pipefoot_halfling
				size = 8750
			}
			create_pop = {
				culture = veykodan
				size = 17500
			}
		}
	}
	s:STATE_NEW_HAVORAL = {
		region_state:B35 = {
			create_pop = {
				culture = new_havoric
				size = 135000
			}
			create_pop = {
				culture = steel_dwarf
				size = 7500
			}
			create_pop = {
				culture = veykodan
				size = 7500
			}
		}
	}
	s:STATE_ARGEZVALE = {
		region_state:B36 = {
			create_pop = {
				culture = steel_dwarf
				size = 225000
			}
			create_pop = {
				culture = veykodan
				size = 45000
			}
			create_pop = {
				culture = sarda
				size = 7500
			}
			create_pop = {
				culture = pipefoot_halfling
				size = 7500
			}
			create_pop = {
				culture = corinsfielder
				size = 15000
			}
		}
	}
	s:STATE_NIZELYNN = {
		region_state:B29 = {
			create_pop = {
				culture = sarda
				size = 160000
			}
			create_pop = {
				culture = epednar
				size = 10000
			}
			create_pop = {
				culture = steadsman
				size = 10000
			}
			create_pop = {
				culture = ynngarder_half_orc
				size = 20000
			}
		}
		region_state:B34 = {
			create_pop = {
				culture = sarda
				size = 20000
			}
			create_pop = {
				culture = epednar
				size = 90000
			}
			create_pop = {
				culture = steadsman
				size = 70000
			}
			create_pop = {
				culture = ynngarder_half_orc
				size = 20000
			}
		}
	}
	s:STATE_CHIPPENGARD = {
		region_state:B30 = {
			create_pop = {
				culture = ynngarder_half_orc
				size = 97500
			}
			create_pop = {
				culture = steadsman
				size = 22500
			}
			create_pop = {
				culture = sarda
				size = 15000
			}
			create_pop = {
				culture = epednar
				size = 15000
			}
		}
	}
	s:STATE_BEGGASLAND = {
		region_state:B34 = {
			create_pop = {
				culture = steadsman
				size = 225000
			}
			create_pop = {
				culture = epednar
				size = 45000
			}
			create_pop = {
				culture = sarda
				size = 30000
			}
		}
	}
	s:STATE_PLUMSTEAD = {
		region_state:B41 = {
			create_pop = {
				culture = steadsman
				size = 210000
				split_religion = {
					steadsman = {
						corinite = 0.05
						ravelian = 0.95
					}
				}
			}
			create_pop = {
				culture = epednar
				religion = ravelian
				size = 70000
			}
			create_pop = {
				culture = sarda
				religion = ravelian
				size = 17500
			}
			create_pop = {
				culture = horizon_elf
				religion = ravelian
				size = 17500
			}
			create_pop = {
				culture = pipefoot_halfling
				size = 35000
				split_religion = {
					pipefoot_halfling = {
						corinite = 0.05
						ravelian = 0.95
					}
				}
			}
		}
	}
	s:STATE_TUSNATA = {
		region_state:B42 = {
			create_pop = {
				culture = steadsman
				size = 68000
			}
			create_pop = {
				culture = epednar
				size = 12000
			}
		}
	}
	s:STATE_BORUCKY = {
		region_state:B40 = {
			create_pop = {
				culture = epednar
				size = 4000
			}
		}
		region_state:B42 = {
			create_pop = {
				culture = steadsman
				size = 48000
			}
			create_pop = {
				culture = epednar
				size = 8000
			}
		}
	}
	s:STATE_ARANTAS = {
		region_state:B40 = {
			create_pop = {
				culture = epednar
				size = 37000
			}
		}
		region_state:B37 = {
			create_pop = {
				culture = horizon_elf
				size = 2000
			}
			create_pop = {
				culture = epednar
				size = 1000
			}
		}
	}
	s:STATE_POSKAWA = {
		region_state:B40 = {
			create_pop = {
				culture = epednar
				size = 6000
			}
		}
		region_state:B43 = {
			create_pop = {
				culture = chiunife
				size = 4800
			}
			create_pop = {
				culture = epednar
				size = 1200
			}
		}
	}
	s:STATE_ELATHAEL = {
		region_state:B37 = {
			create_pop = {
				culture = horizon_elf
				size = 126000
			}
			create_pop = {
				culture = dustman
				size = 27000
			}
			create_pop = {
				culture = epednar
				size = 14000
			}
		}
		region_state:B40 = {
			create_pop = {
				culture = epednar
				size = 13000
			}
		}
	}
	s:STATE_EPADARKAN = {
		region_state:B46 = {
			create_pop = {
				culture = sarda
				size = 125000
			}
			create_pop = {
				culture = dustman
				size = 5000
			}
		}
		region_state:B45 = {
			create_pop = {
				culture = sarda
				size = 100000
			}
			create_pop = {
				culture = dustman
				size = 20000
			}
		}
	}
	s:STATE_TELLUMTIR = {
		region_state:B91 = {
			create_pop = {
				culture = dustman
				size = 96000
			}
			create_pop = {
				culture = epednar
				size = 24000
			}
		}
	}
	s:STATE_UZOO = {
		region_state:B91 = {
			create_pop = {
				culture = dustman
				size = 8000
			}
			create_pop = {
				culture = epednar
				size = 18000
			}
		}
		region_state:B40 = {
			create_pop = {
				culture = epednar
				size = 54000
			}
		}
	}
	s:STATE_ARGANJUZORN = {
		region_state:B47 = {
			create_pop = {
				culture = dolindhan
				size = 205000
			}
			create_pop = {
				culture = epednar
				size = 15000
			}
			create_pop = {
				culture = dustman
				size = 15000
			}
			create_pop = {
				culture = sarda
				size = 5000
			}
			create_pop = {
				culture = tuathak
				size = 30000
			}
		}
		region_state:B46 = {
			create_pop = {
				culture = sarda
				size = 25000
			}
			create_pop = {
				culture = dolindhan
				size = 5000
			}
		}
	}
	s:STATE_EBENMAS = {
		region_state:B91 = {
			create_pop = {
				culture = dustman
				size = 119000
			}
			create_pop = {
				culture = epednar
				size = 21000
			}
		}
	}
	s:STATE_YECKUNIA = {
		region_state:B91 = {
			create_pop = {
				culture = dustman
				size = 3600
			}
			create_pop = {
				culture = epednar
				size = 600
			}
		}
		region_state:B43 = {
			create_pop = {
				culture = chiunife
				size = 16800
			}
		}
		region_state:B40 = {
			create_pop = {
				culture = epednar
				size = 3000
			}
		}
	}
	s:STATE_CASNAROG = {
		region_state:B43 = {
			create_pop = {
				culture = chiunife
				size = 11700
			}
			create_pop = {
				culture = epednar
				size = 1300
			}
		}
	}
	s:STATE_POMVASONN = {
		region_state:B49 = {
			create_pop = {
				culture = rzentur
				size = 720000
			}
			create_pop = {
				culture = epednar
				size = 40000
			}
			create_pop = {
				culture = vrendzani_kobold
				size = 40000
				pop_type = slaves
				split_religion = {
					vrendzani_kobold = {
						the_thought = 0.1
						drozma_tur = 0.9
					}
				}
			}
		}
	}
	s:STATE_MOCEPED = {
		region_state:B49 = {
			create_pop = {
				culture = dolindhan
				size = 233750
			}
			create_pop = {
				culture = rzentur
				size = 41250
			}
		}
	}
	s:STATE_DROZMADREMOV = {
		region_state:B49 = {
			create_pop = {
				culture = rzentur
				size = 810000
			}
			create_pop = {
				culture = rzentur
				religion = vechnogejzn
				size = 40000
			}
			create_pop = {
				culture = istranari
				size = 50000
			}
			create_pop = {
				culture = vrendzani_kobold
				size = 100000
				pop_type = slaves
				split_religion = {
					vrendzani_kobold = {
						the_thought = 0.1
						drozma_tur = 0.9
					}
				}
			}
		}
	}
	s:STATE_GOMOSENGHA = {
		region_state:B49 = {
			create_pop = {
				culture = rzentur
				size = 360000
			}
			create_pop = {
				culture = rzentur
				religion = vechnogejzn
				size = 20000
			}
			create_pop = {
				culture = vrendzani_kobold
				size = 20000
				pop_type = slaves
				split_religion = {
					vrendzani_kobold = {
						the_thought = 0.1
						drozma_tur = 0.9
					}
				}
			}
		}
		region_state:B50 = {
			create_pop = {
				culture = rzentur
				religion = vechnogejzn
				size = 2000
			}
		}
	}
	s:STATE_ARSERGOZHAR = {
		region_state:B50 = {
			create_pop = {
				culture = rzentur
				religion = vechnogejzn
				size = 25000
			}
		}
	}
	s:STATE_JUZONDEZAN = {
		region_state:B49 = {
			create_pop = {
				culture = rzentur
				size = 420000
			}
			create_pop = {
				culture = dolindhan
				size = 90000
			}
			create_pop = {
				culture = istranari
				size = 60000
			}
			create_pop = {
				culture = vrendzani_kobold
				size = 30000
				pop_type = slaves
				split_religion = {
					vrendzani_kobold = {
						the_thought = 0.1
						drozma_tur = 0.9
					}
				}
			}
		}
	}
	s:STATE_USLAD = {
		region_state:B49 = {
			create_pop = {
				culture = dolindhan
				size = 75000
			}
			create_pop = {
				culture = rzentur
				size = 25000
			}
		}
		region_state:B39 = {
			create_pop = {
				culture = dolindhan
				size = 350000
			}
			create_pop = {
				culture = rzentur
				size = 10000
			}
			create_pop = {
				culture = sarda
				size = 50000
			}
			create_pop = {
				culture = corinsfielder
				size = 10000
			}
			create_pop = {
				culture = pipefoot_halfling
				size = 10000
			}
			create_pop = {
				culture = cliff_gnome
				size = 35000
			}
		}
		region_state:B52 = {
			create_pop = {
				culture = dolindhan
				size = 125000
			}
		}
	}
	s:STATE_ROGAIDHA = {
		region_state:B49 = {
			create_pop = {
				culture = dolindhan
				size = 675000
			}
			create_pop = {
				culture = rzentur
				size = 75000
			}
		}
	}
	s:STATE_BRELAR = {
		region_state:B49 = {
			create_pop = {
				culture = rzentur
				size = 25000
			}
			create_pop = {
				culture = dolindhan
				size = 225000
			}
		}
		region_state:B51 = {
			create_pop = {
				culture = cursed_one
				size = 13600
			}
		}
	}
	s:STATE_VERZEL = {
		region_state:B49 = {
			create_pop = {
				culture = rzentur
				size = 90000
			}
			create_pop = {
				culture = dolindhan
				size = 180000
			}
			create_pop = {
				culture = istranari
				size = 30000
			}
		}
	}
	s:STATE_VITREYNN = {
		region_state:B38 = {
			create_pop = {
				culture = dolindhan
				size = 450000
			}
		}
	}
	s:STATE_VARBUKLAND = {
		region_state:B53 = {
			create_pop = {
				culture = freemarcher_half_orc
				size = 200000
			}
			create_pop = {
				culture = cursed_one
				size = 50000
			}
		}
	}
	s:STATE_KEWDHEMR = {
		region_state:B54 = {
			create_pop = {
				culture = cursed_one
				size = 9150
			}
		}
		region_state:B55 = {
			create_pop = {
				culture = cursed_one
				size = 8650
			}
		}
		region_state:B57 = {
			create_pop = {
				culture = cursed_one
				size = 1200
			}
		}
		region_state:B53 = {
			create_pop = {
				culture = freemarcher_half_orc
				size = 8400
			}
			create_pop = {
				culture = cursed_one
				size = 5600
			}
		}
	}
	s:STATE_ZEMSHOGEN = {
		region_state:B51 = {
			create_pop = {
				culture = cursed_one
				size = 17040
			}
		}
		region_state:A06 = {
			create_pop = {
				culture = cliff_gnome
				size = 680
			}
			create_pop = {
				culture = themarian
				size = 3580
			}
			create_pop = {
				culture = reverian
				size = 2800
			}
		}
	}
	s:STATE_WHETJUL = {
		region_state:B56 = {
			create_pop = {
				culture = cursed_one
				size = 14500
			}
		}
		region_state:B57 = {
			create_pop = {
				culture = cursed_one
				size = 2500
			}
		}
	}
	s:STATE_NEFRNMEHN = {
		region_state:B57 = {
			create_pop = {
				culture = cursed_one
				size = 7050
			}
		}
		region_state:B58 = {
			create_pop = {
				culture = cursed_one
				size = 6060
			}
		}
		region_state:A06 = {
			create_pop = {
				culture = cliff_gnome
				size = 400
			}
			create_pop = {
				culture = themarian
				size = 2790
			}
			create_pop = {
				culture = reverian
				size = 2600
			}
		}
	}
	s:STATE_HEHADEDPAR = {
		region_state:B63 = {
			create_pop = {
				culture = cursed_one
				size = 7060
			}
		}
		region_state:B58 = {
			create_pop = {
				culture = cursed_one
				size = 10300
			}
		}
		region_state:A06 = {
			create_pop = {
				culture = cliff_gnome
				size = 860
			}
			create_pop = {
				culture = themarian
				size = 5280
			}
			create_pop = {
				culture = reverian
				size = 4500
			}
		}
	}
}