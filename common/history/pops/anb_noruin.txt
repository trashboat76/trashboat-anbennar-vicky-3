﻿POPS = {
	s:STATE_KWINELLIANDE = {
		region_state:B18 = { #Most "even" in terms of proportion
			create_pop = {
				culture = bloodgrover
				size = 759000
				split_religion = {
					bloodgrover = {
						regent_court = 0.7
						ravelian = 0.3
					}
				}
			}
			create_pop = {
				culture = moon_elven
				size = 282600
				split_religion = {
					moon_elven = {
						regent_court = 0.5
						ravelian = 0.5
					}
				}
			}
			create_pop = {
				culture = sornicandi
				size = 340000
				split_religion = {
					sornicandi = {
						regent_court = 0.2
						ravelian = 0.8
					}
				}
			}
			create_pop = {
				culture = kwineh
				size = 47100
				split_religion = {
					kwineh = {
						regent_court = 0.2
						ravelian = 0.8
					}
				}
			}
			create_pop = {
				culture = bloodfog_orc
				size = 123638
				pop_type = slaves
			}
			create_pop = {
				culture = bloodfog_orc
				size = 17662
			}
		}
	}
	s:STATE_NURTHANNCOST = {
		region_state:B18 = { #High Lorentish majority
			create_pop = {
				culture = bloodgrover
				size = 573520
				split_religion = {
					bloodgrover = {
						regent_court = 0.5
						ravelian = 0.5
					}
				}
			}
			create_pop = {
				culture = moon_elven
				size = 136960
				split_religion = {
					moon_elven = {
						regent_court = 0.2
						ravelian = 0.8
					}
				}
			}
			create_pop = {
				culture = kwineh
				size = 34240
				split_religion = {
					kwineh = {
						regent_court = 0.1
						ravelian = 0.9
					}
				}
			}
			create_pop = {
				culture = bloodfog_orc
				size = 97370
				pop_type = slaves
			}
			create_pop = {
				culture = bloodfog_orc
				size = 13910
			}
		}
	}
	s:STATE_DEATHWOODS = {
		region_state:B18 = { #Elven majority
			create_pop = {
				culture = bloodgrover
				size = 308000
				split_religion = {
					bloodgrover = {
						regent_court = 0.3
						ravelian = 0.7
					}
				}
			}
			create_pop = {
				culture = moon_elven
				size = 102310
				split_religion = {
					moon_elven = {
						regent_court = 0.2
						ravelian = 0.8
					}
				}
			}
			create_pop = {
				culture = kwineh
				size = 39350
				religion = ravelian
			}
			create_pop = {
				culture = sornicandi
				size = 250770
				religion = ravelian
			}
			create_pop = {
				culture = bloodfog_orc
				size = 74202
				pop_type = slaves
			}
			create_pop = {
				culture = bloodfog_orc
				size = 12367
			}
		}
	}
	s:STATE_SORNICANDE = {
		region_state:B18 = { #Sornicandi heartland
			create_pop = {
				culture = sornicandi
				size = 501000
				religion = ravelian
			}
			create_pop = {
				culture = kwineh
				size = 163800
				religion = ravelian
			}
			create_pop = {
				culture = bloodgrover
				size = 201000
				split_religion = {
					bloodgrover = {
						regent_court = 0.4
						ravelian = 0.6
					}
				}
			}
			create_pop = {
				culture = moon_elven
				size = 94500
				split_religion = {
					moon_elven = {
						regent_court = 0.2
						ravelian = 0.8
					}
				}
			}
			create_pop = {
				culture = bloodfog_orc
				size = 99750
				pop_type = slaves
			}
			create_pop = {
				culture = bloodfog_orc
				size = 19950
			}
		}
	}
	s:STATE_SOUTH_DALAIRE = {
		region_state:B17 = {
			create_pop = {
				culture = star_elf
				size = 39600
			}
		}
		region_state:B18 = { #Kwineh mainly migrated there
			create_pop = {
				culture = kwineh
				size = 85200
				religion = ravelian
			}
			create_pop = {
				culture = sornicandi
				size = 93600
				religion = ravelian
			}
			create_pop = {
				culture = bloodfog_orc
				size = 18000
				pop_type = slaves
			}
			create_pop = {
				culture = bloodfog_orc
				size = 3600
			}
		}
	}
	s:STATE_CESTIRMARK = {
		region_state:B98 = {
			create_pop = {
				culture = concordian
				size = 915120
			}
			create_pop = {
				culture = boek
				size = 7380
				religion = ravelian
			}
			create_pop = {
				culture = towerfoot_halfling
				size = 123000
			}
			create_pop = {
				culture = cannorian_half_orc
				size = 24600
				religion = ravelian
			}
			create_pop = {
				culture = undercliff_orc
				size = 133250
				pop_type = slaves
			}
			create_pop = {
				culture = undercliff_orc
				size = 26650
			}
		}
	}
	s:STATE_UPPER_SELLA = {
		region_state:B18 = {
			create_pop = {	#Yanek tribe was peacefully annexed so they may still be well off
				culture = boek
				size = 209080
				split_religion = {
					boek = {
						ravelian = 0.8
						dobondotist = 0.2
					}
				}
			}
			create_pop = {
				culture = concordian
				size = 68240
			}
			create_pop = {
				culture = undercliff_orc
				size = 40200
				pop_type = slaves
			}
			create_pop = {
				culture = undercliff_orc
				size = 8040
			}
		}
		region_state:B98 = {
			create_pop = {
				culture = boek
				size = 80200
				split_religion = {
					boek = {
						ravelian = 0.8
						dobondotist = 0.2
					}
				}
			}
			create_pop = {
				culture = concordian
				size = 45320
			}
			create_pop = {
				culture = undercliff_orc
				size = 46200
				pop_type = slaves
			}
			create_pop = {
				culture = undercliff_orc
				size = 8040
			}
			
		}
	}
	s:STATE_TROLLSBRIDGE = {
		region_state:B98 = {
			create_pop = {
				culture = concordian
				size = 665028
			}
			create_pop = {
				culture = boek
				size = 6032
				split_religion = {
					boek = {
						ravelian = 0.8
						dobondotist = 0.2
					}
				}
			}
			create_pop = {
				culture = towerfoot_halfling
				size = 15080
			}
			create_pop = {
				culture = undercliff_orc
				size = 56550
				pop_type = slaves
			}
			create_pop = {
				culture = undercliff_orc
				size = 11310
			}
		}
	}
	s:STATE_SOUTH_MARLLIANDE = {
		region_state:B98 = {
			create_pop = {
				culture = concordian
				size = 485520
				split_religion = {
					concordian = {
						regent_court = 0.6
						ravelian = 0.4
					}
				}
			}
			create_pop = {
				culture = boek
				size = 8670
				split_religion = {
					boek = {
						regent_court = 0.2
						ravelian = 0.6
						dobondotist = 0.2
					}
				}
			}
			create_pop = {
				culture = towerfoot_halfling
				size = 52020
			}
			create_pop = {
				culture = undercliff_orc
				size = 320250
				pop_type = slaves
			}
			create_pop = {
				culture = undercliff_orc
				size = 540
			}
		}
	}
	s:STATE_NORTH_MARLLIANDE = {
		region_state:B98 = {
			create_pop = {
				culture = concordian
				size = 103800
				split_religion = {
					concordian = {
						regent_court = 0.75
						ravelian = 0.25
					}
				}
			}
			create_pop = {
				culture = boek
				size = 7200
				split_religion = {
					boek = {
						regent_court = 0.6
						dobondotist = 0.4
					}
				}
			}
			create_pop = {
				culture = undercliff_orc
				size = 186885
				pop_type = slaves
			}
			create_pop = {
				culture = undercliff_orc
				size = 2115
			}
		}
	}
	s:STATE_YNNSMOUTH = {
		region_state:B21 = {
			create_pop = {
				culture = boek
				size = 420060
			}
			create_pop = {
				culture = mouther
				size = 180100
			}
			create_pop = {
				culture = valorborn
				size = 20800
			}
			create_pop = {
				culture = sarda
				size = 40300
			}
			create_pop = {
				culture = concordian
				size = 7830
			}
		}
	}
	s:STATE_ZANLIB = {
		region_state:B98 = {
			create_pop = {
				culture = zanlibi
				size = 822160
			}
			create_pop = {
				culture = towerfoot_halfling
				size = 34400
			}
			create_pop = {
				culture = boek
				size = 3440
				split_religion = {
					boek = {
						old_sun_cult = 0.8
						dobondotist = 0.2
					}
				}
			}
		}
	}
	s:STATE_ISOBELIN = {
		region_state:B98 = {
			create_pop = {
				culture = concordian
				size = 1555702
			}
			create_pop = {	#Jayek tribe, peacefully integrated
				culture = boek
				size = 171000
				religion = ravelian
			}
			create_pop = {	#Cheshoshi mercenaries mentioned in an idea
				culture = cheshoshi
				size = 10040
				religion = ravelian
			}
			create_pop = {
				culture = moon_elven
				size = 13578
				religion = ravelian
			}
			create_pop = {
				culture = towerfoot_halfling
				size = 271560
			}
			create_pop = {
				culture = cannorian_half_orc
				size = 113150
				religion = ravelian
			}
			create_pop = {
				culture = undercliff_orc
				size = 45260
			}
			create_pop = {
				culture = valorborn
				size = 22630
			}
			create_pop = {
				culture = zanlibi
				size = 60080
			}
			create_pop = {
				culture = firanyan_harpy
				size = 564
				religion = ravelian
			}
		}
	}
	s:STATE_THILVIS = {
		region_state:B98 = {
			create_pop = {
				culture = towerfoot_halfling
				size = 1022000
			}
			create_pop = {
				culture = boek
				size = 7000
				religion = ravelian
			}
			create_pop = {
				culture = concordian
				size = 50000
				split_religion = {
					concordian = {
						corinite = 0.4
						ravelian = 0.6
					}
				}
			}
			create_pop = {
				culture = concordian
				size = 80960
			}
			create_pop = {
				culture = concordian
				size = 100740
			}
			create_pop = {
				culture = undercliff_orc
				size = 261335
				pop_type = slaves
			}
			create_pop = {
				culture = undercliff_orc
				size = 32665
			}
			create_pop = {
				culture = tinker_gnome
				size = 5220
			}
			create_pop = {
				culture = firanyan_harpy
				size = 1200
				religion = ravelian
			}
		}
	}
	s:STATE_BOEK = {
		region_state:B98 = {
			create_pop = {
				culture = towerfoot_halfling
				size = 170400
			}
			create_pop = {	#Boek tribe got heavily repressed, so they may be a minority
				culture = boek
				size = 5100
				split_religion = {
					boek = {
						ravelian = 0.6
						dobondotist = 0.4
					}
				}
			}
			create_pop = {
				culture = undercliff_orc
				size = 86900
				pop_type = slaves
			}
			create_pop = {
				culture = undercliff_orc
				size = 6700
			}
		}
		region_state:B89 = {
			create_pop = {
				culture = concordian
				size = 10800
			}
			create_pop = {
				culture = towerfoot_halfling
				size = 60000
			}
			create_pop = {
				culture = boek
				size = 20100
				split_religion = {
					boek = {
						ravelian = 0.4
						dobondotist = 0.6
					}
				}
			}
		}
	}
	s:STATE_VALORPOINT = {
		region_state:B98 = {
			create_pop = {
				culture = valorborn
				size = 381300
				split_religion = {
					valorborn = {
						corinite = 0.7
						ravelian = 0.3
					}
				}
			}
			create_pop = {	#Lots of refugees from the rest of the Trollsbay
				culture = boek
				size = 34000
				split_religion = {
					boek = {
						corinite = 0.6
						ravelian = 0.2
						dobondotist = 0.2
					}
				}
			}
			create_pop = {
				culture = cheshoshi
				size = 10400
				split_religion = {
					cheshoshi = {
						corinite = 0.7
						ravelian = 0.3
					}
				}
			}
			create_pop = {
				culture = concordian
				size = 280200
				split_religion = {
					concordian = {
						corinite = 0.7
						ravelian = 0.3
					}
				}
			}
			create_pop = {
				culture = undercliff_orc
				size = 50400
			}
		}
		region_state:B26 = {
			create_pop = {
				culture = valorborn
				size = 121000
				split_religion = {
					valorborn = {
						corinite = 0.7
						ravelian = 0.3
					}
				}
			}
			create_pop = {
				culture = concordian
				size = 30000
				split_religion = {
					concordian = {
						corinite = 0.7
						ravelian = 0.3
					}
				}
			}
			create_pop = {
				culture = boek
				size = 16000
				split_religion = {
					boek = {
						corinite = 0.4
						ravelian = 0.6
					}
				}
			}
			create_pop = {
				culture = cheshoshi
				size = 14000
				split_religion = {
					cheshoshi = {
						corinite = 0.5
						ravelian = 0.5
					}
				}
			}
			create_pop = {
				culture = ionnidari
				size = 6000
			}
			create_pop = {
				culture = undercliff_orc
				size = 5400
				pop_type = slaves
			}
		}
	}
	s:STATE_OKHIBOLI = {
		region_state:B26 = {
			create_pop = {
				culture = cheshoshi
				size = 387600
				split_religion = {
					cheshoshi = {
						ravelian = 0.9
						regent_court = 0.1
					}
				}
			}
			create_pop = {
				culture = ionnidari
				size = 156750
				split_religion = {
					ionnidari = {
						ravelian = 0.3
						regent_court = 0.7
					}
				}
			}
			create_pop = {
				culture = ruin_orc
				size = 24443
				pop_type = slaves
			}
			create_pop = {
				culture = ruin_orc
				size = 1206
			}
		}
	}
	s:STATE_SPOORLAND = {
		region_state:B26 = {
			create_pop = {
				culture = cheshoshi
				size = 102100
				split_religion = {
					cheshoshi = {
						ravelian = 0.75
						death_cult_of_cheshosh = 0.25
					}
				}
			}
			create_pop = {
				culture = valorborn
				size = 16060
				religion = ravelian
			}
			create_pop = {
				culture = ionnidari
				size = 17200
				split_religion = {
					ionnidari = {
						ravelian = 0.4
						regent_court = 0.6
					}
				}
			}
			create_pop = {
				culture = towerfoot_halfling
				size = 317600
			}
			create_pop = {
				culture = undercliff_orc
				size = 29347
				pop_type = slaves
			}
			create_pop = {
				culture = undercliff_orc
				size = 4192
			}
		}
	}
	s:STATE_GRAVEROADS = {
		region_state:B27 = {
			create_pop = {
				culture = neratican
				size = 655320
			}
			create_pop = {
				culture = cheshoshi
				size = 152400
				religion = regent_court
			}
			create_pop = {
				culture = ruin_orc
				size = 76200
				pop_type = slaves
				split_religion = {
					ruin_orc = {
						bulgu_orazan = 0.2
						regent_court = 0.8
					}
				}
			}
			create_pop = {
				culture = ruin_orc
				size = 15240
				religion = regent_court
			}
		}
	}
	s:STATE_NERATICA = {
		region_state:B27 = {
			create_pop = {
				culture = neratican
				size = 834440
			}
			create_pop = {
				culture = cheshoshi
				size = 116900
				religion = regent_court
			}
			create_pop = {
				culture = ruin_orc
				size = 61335
				pop_type = slaves
				split_religion = {
					ruin_orc = {
						bulgu_orazan = 0.15
						regent_court = 0.85
					}
				}
			}
			create_pop = {
				culture = ruin_orc
				size = 12265
				religion = regent_court
			}
		}
	}
	s:STATE_DISHENTI = {
		region_state:B28 = {
			create_pop = {
				culture = cheshoshi
				size = 64600
			}
		}
		region_state:B27 = {
			create_pop = {
				culture = neratican
				size = 10400
			}
			create_pop = {
				culture = cheshoshi
				size = 3600
				split_religion = {
					cheshoshi = {
						regent_court = 0.8
						death_cult_of_cheshosh = 0.2
					}
				}
			}
		}
	}
	s:STATE_DARKGROVES = {
		region_state:B27 = {
			create_pop = {
				culture = neratican
				size = 82800
			}
			create_pop = {
				culture = cheshoshi
				size = 80960
				religion = regent_court
			}
			create_pop = {
				culture = ruin_orc
				size = 24900
				pop_type = slaves
				split_religion = {
					ruin_orc = {
						bulgu_orazan = 0.5
						regent_court = 0.5
					}
				}
			}
			create_pop = {
				culture = ruin_orc
				size = 4980
				religion = regent_court
			}
		}
		region_state:B28 = {
			create_pop = {
				culture = cheshoshi
				size = 8700
			}
		}
	}
	s:STATE_MORDUN = {
		region_state:B27 = {
			create_pop = {	#Largest Cheshoshi minority due to Soru tribe being spared
				culture = cheshoshi
				size = 189362
				religion = regent_court
			}
			create_pop = {
				culture = neratican
				size = 61064
			}
			create_pop = {
				culture = ruin_orc
				size = 32434
				pop_type = slaves
				split_religion = {
					ruin_orc = {
						bulgu_orazan = 0.7
						regent_court = 0.3
					}
				}
			}
			create_pop = {
				culture = ruin_orc
				size = 5405
				religion = regent_court
			}
		}
	}
}