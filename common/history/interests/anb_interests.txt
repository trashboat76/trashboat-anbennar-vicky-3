INTERESTS = {
	c:A01 = { #Anbennar
		add_declared_interest = region_adenica
		add_declared_interest = region_dostanor
		add_declared_interest = region_amadia
		add_declared_interest = region_north_lencenor
	}
	
	c:A02 = { #Vivin Empire - Just regions in their general vicinity
		add_declared_interest = region_the_marches
		add_declared_interest = region_bahar
		add_declared_interest = region_businor
		add_declared_interest = region_adenica
	}
	
	c:A03 = { #Lorent
		add_declared_interest = region_northern_dameshead
		add_declared_interest = region_dragon_coast
		add_declared_interest = region_eastern_dameshead
		add_declared_interest = region_businor
		add_declared_interest = region_south_rahen
		add_declared_interest = region_thidinkai
		add_declared_interest = region_lupulan
	}

	c:A04 = { #Northern League
		add_declared_interest = region_bulwar_proper
		add_declared_interest = region_adenica
		add_declared_interest = region_north_lencenor
	}

	c:A10 = { #Grombar
		add_declared_interest = region_alen
		add_declared_interest = region_dragon_coast
		add_declared_interest = region_south_lencenor
		add_declared_interest = region_adenica
		add_declared_interest = region_triunic_lakes
		add_declared_interest = region_south_plains
		add_declared_interest = region_east_yanshen
		add_declared_interest = region_eastern_dameshead
		add_declared_interest = region_businor
	}

	c:B98 = { #Trollsbay
		add_declared_interest = region_haraf
		add_declared_interest = region_tor_nayyi
		add_declared_interest = region_lower_ynn
		add_declared_interest = region_epednan_expanse
		add_declared_interest = region_dalaire
	}
}
