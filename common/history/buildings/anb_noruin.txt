﻿BUILDINGS={
	s:STATE_CESTIRMARK={
		region_state:B98={
			create_building={
				building="building_maize_farm"
				level=4
				reserves=1
				activate_production_methods={ "pm_simple_farming" "pm_no_secondary" "pm_tools" "pm_privately_owned" }
			}
			create_building={
				building="building_logging_camp"
				level=4
				reserves=1
				activate_production_methods={ "pm_saw_mills" "pm_merchant_guilds_building_logging_camp" "pm_no_equipment" "pm_road_carts" "pm_no_hardwood" }
			}
			create_building={
				building="building_fishing_wharf"
				level=1
				reserves=1
				activate_production_methods={ "pm_merchant_guilds_building_fishing_wharf" "pm_unrefrigerated" "pm_fishing_trawlers" }
			}
			create_building={
				building="building_iron_mine"
				level=2
				reserves=1
				activate_production_methods={ "pm_atmospheric_engine_pump_building_iron_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" "pm_privately_owned_building_iron_mine" }
			}
			create_building={
				building="building_glassworks"
				level=1
				reserves=1
				activate_production_methods={ "pm_leaded_glass"  "pm_ceramics" "pm_manual_glassblowing" "pm_privately_owned_building_glassworks" }
			}
			create_building={
				building="building_textile_mills"
				level=3
				reserves=1
				activate_production_methods={ "pm_handsewn_clothes" "pm_no_luxury_clothes" "pm_traditional_looms" "pm_merchant_guilds_building_textile_mills" }
			}
			create_building={
				building="building_university"
				level=1
				reserves=1
				activate_production_methods={ "pm_scholastic_education" }
			}
			create_building={
				building="building_government_administration"
				level=3
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" }
			}
			create_building={
				building="building_cotton_plantation"
				level=4
				reserves=1
				activate_production_methods={ "default_building_cotton_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_tobacco_plantation"
				level=4
				reserves=1
				activate_production_methods={ "default_building_tobacco_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_barracks"
				level=4
				reserves=1
				activate_production_methods={ "pm_no_organization" "pm_mobile_artillery" }
			}
			create_building={
				building="building_naval_base"
				level=3
				reserves=1
				activate_production_methods={ "pm_no_naval_theory" }
			}
			create_building={
				building="building_port"
				level=2
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
			create_building={
				building="building_construction_sector"
				level=1
				reserves=1
				activate_production_methods={ "pm_wooden_buildings" }
			}
		}
	}
	s:STATE_UPPER_SELLA={
		REGION_STATE:B18={
			create_building={
				building="building_cotton_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_cotton_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_coal_mine"
				level=1
				reserves=1
				activate_production_methods={ "pm_atmospheric_engine_pump_building_coal_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" "pm_privately_owned_building_coal_mine" }
			}
		}
		REGION_STATE:B98={
			create_building={
				building="building_maize_farm"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_farming" "pm_no_secondary" "pm_tools" "pm_privately_owned" }
			}
			create_building={
				building="building_cotton_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_cotton_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_tobacco_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_tobacco_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
		}
	}
	s:STATE_TROLLSBRIDGE={
		region_state:B98={
			create_building={
				building="building_logging_camp"
				level=2
				reserves=1
				activate_production_methods={ "pm_saw_mills" "pm_merchant_guilds_building_logging_camp" "pm_no_equipment" "pm_road_carts" "pm_no_hardwood" }
			}
			create_building={
				building="building_maize_farm"
				level=2
				reserves=1
				activate_production_methods={ "pm_simple_farming" "pm_no_secondary" "pm_tools" "pm_privately_owned" }
			}
			create_building={
				building="building_livestock_ranch"
				level=1
				reserves=1
				activate_production_methods={ "pm_privately_owned" "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_open_air_stockyards" }
			}
			create_building={
				building="building_fishing_wharf"
				level=2
				reserves=1
				activate_production_methods={ "pm_merchant_guilds_building_fishing_wharf" "pm_unrefrigerated" "pm_fishing_trawlers" }
			}
			create_building={
				building="building_food_industry"
				level=1
				reserves=1
				activate_production_methods={ "pm_merchant_guilds_building_food_industry" "pm_disabled_canning" "pm_bakery" "pm_manual_dough_processing" "pm_disabled_distillery" }
			}
			create_building={
				building="building_tobacco_plantation"
				level=3
				reserves=1
				activate_production_methods={ "default_building_tobacco_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_government_administration"
				level=1
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" }
			}
			create_building={
				building="building_barracks"
				level=1
				reserves=1
				activate_production_methods={ "pm_no_organization" "pm_mobile_artillery" }
			}
			create_building={
				building="building_naval_base"
				level=1
				reserves=1
				activate_production_methods={ "pm_no_naval_theory" }
			}
			create_building={
				building="building_port"
				level=1
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
			create_building={
				building="building_construction_sector"
				level=1
				reserves=1
				activate_production_methods={ "pm_wooden_buildings" }
			}
		}
	}
	s:STATE_SOUTH_MARLLIANDE={
		region_state:B98={
			create_building={
				building="building_maize_farm"
				level=4
				reserves=1
				activate_production_methods={ "pm_simple_farming" "pm_vineyards_building_maize_farm" "pm_tools" "pm_privately_owned" }
			}
			create_building={
				building="building_cotton_plantation"
				level=10
				reserves=1
				activate_production_methods={ "default_building_cotton_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_tobacco_plantation"
				level=5
				reserves=1
				activate_production_methods={ "default_building_tobacco_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_logging_camp"
				level=1
				reserves=1
				activate_production_methods={ "pm_saw_mills" "pm_merchant_guilds_building_logging_camp" "pm_no_equipment" "pm_road_carts" "pm_no_hardwood" }
			}
			create_building={
				building="building_furniture_manufacturies"
				level=1
				reserves=1
				activate_production_methods={ "pm_lathe" "pm_merchant_guilds_building_furniture_manufacturies" "pm_automation_disabled" "pm_luxury_furniture" }
			}
			create_building={
				building="building_university"
				level=1
				reserves=1
				activate_production_methods={ "pm_scholastic_education" }
			}
			create_building={
				building="building_government_administration"
				level=2
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" }
			}
			create_building={
				building="building_barracks"
				level=3
				reserves=1
				activate_production_methods={ "pm_no_organization" "pm_mobile_artillery" }
			}
			create_building={
				building="building_naval_base"
				level=3
				reserves=1
				activate_production_methods={ "pm_no_naval_theory" }
			}
			create_building={
				building="building_port"
				level=1
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
		}
	}
	s:STATE_NORTH_MARLLIANDE={
		region_state:B98={
			create_building={
				building="building_maize_farm"
				level=2
				reserves=1
				activate_production_methods={ "pm_simple_farming" "pm_vineyards_building_maize_farm" "pm_tools" "pm_privately_owned" }
			}
			create_building={
				building="building_cotton_plantation"
				level=5
				reserves=1
				activate_production_methods={ "default_building_cotton_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_tobacco_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_tobacco_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_logging_camp"
				level=1
				reserves=1
				activate_production_methods={ "pm_saw_mills" "pm_merchant_guilds_building_logging_camp" "pm_no_equipment" "pm_road_carts" "pm_no_hardwood" }
			}
		}
	}
	s:STATE_YNNSMOUTH={
		region_state:B21={
			create_building={
				building="building_rice_farm"
				level=3
				reserves=1
				activate_production_methods={ "pm_simple_farming_building_rice_farm" "pm_privately_owned" "pm_tools_disabled" "pm_fig_orchards_building_rice_farm" }
			}
			create_building={
				building="building_fishing_wharf"
				level=2
				reserves=1
				activate_production_methods={ "pm_merchant_guilds_building_fishing_wharf" "pm_unrefrigerated" "pm_fishing_trawlers" }
			}
			create_building={
				building="building_cotton_plantation"
				level=5
				reserves=1
				activate_production_methods={ "default_building_cotton_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_textile_mills"
				level=1
				reserves=1
				activate_production_methods={ "pm_handsewn_clothes" "pm_no_luxury_clothes" "pm_traditional_looms" "pm_merchant_guilds_building_textile_mills" }
			}
			create_building={
				building="building_government_administration"
				level=1
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" }
			}
			create_building={
				building="building_barracks"
				level=1
				reserves=1
				activate_production_methods={ "pm_no_organization" "pm_mobile_artillery" }
			}
			create_building={
				building="building_naval_base"
				level=2
				reserves=1
				activate_production_methods={ "pm_no_naval_theory" }
			}
			create_building={
				building="building_port"
				level=1
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
		}
	}
	s:STATE_ZANLIB={ #add mage stuff later
		REGION_STATE:B98={
			create_building={
				building="building_maize_farm"
				level=3
				reserves=1
				activate_production_methods={ "pm_simple_farming" "pm_citrus_orchards" "pm_tools" "pm_privately_owned" }
			}
			create_building={
				building="building_lead_mine"
				level=1
				reserves=1
				activate_production_methods={ "pm_atmospheric_engine_pump_building_lead_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" "pm_privately_owned_building_lead_mine" }
			}
			create_building={
				building="building_logging_camp"
				level=1
				reserves=1
				activate_production_methods={ "pm_saw_mills" "pm_merchant_guilds_building_logging_camp" "pm_no_equipment" "pm_road_carts" "pm_no_hardwood" }
			}
			create_building={
				building="building_shipyards"
				level=1
				reserves=1
				activate_production_methods={ "pm_basic_shipbuilding" "pm_merchant_guilds_building_shipyards" "pm_military_shipbuilding_wooden" }
			}
			create_building={
				building="building_glassworks"
				level=2
				reserves=1
				activate_production_methods={ "pm_leaded_glass" "pm_ceramics" "pm_manual_glassblowing" "pm_privately_owned_building_glassworks" }
			}
			create_building={
				building="building_paper_mills"
				level=3
				reserves=1
				activate_production_methods={ "pm_pulp_pressing" "pm_automation_disabled" "pm_privately_owned_building_paper_mills" }
			}
			create_building={
				building="building_cotton_plantation"
				level=4
				reserves=1
				activate_production_methods={ "default_building_cotton_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_tobacco_plantation"
				level=2
				reserves=1
				activate_production_methods={ "default_building_tobacco_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_government_administration"
				level=1
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" }
			}
			create_building={
				building="building_barracks"
				level=4
				reserves=1
				activate_production_methods={ "pm_no_organization" "pm_mobile_artillery" }
			}
			create_building={
				building="building_naval_base"
				level=5
				reserves=1
				activate_production_methods={ "pm_no_naval_theory" }
			}
			create_building={
				building="building_port"
				level=2
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
		}
	}
	s:STATE_ISOBELIN={
		REGION_STATE:B98={
			create_building={
				building="building_maize_farm"
				level=3
				reserves=1
				activate_production_methods={ "pm_simple_farming" "pm_no_secondary" "pm_tools" "pm_privately_owned" }
			}
			create_building={
				building="building_fishing_wharf"
				level=3
				reserves=1
				activate_production_methods={ "pm_merchant_guilds_building_fishing_wharf" "pm_unrefrigerated" "pm_fishing_trawlers" }
			}
			create_building={
				building="building_logging_camp"
				level=2
				reserves=1
				activate_production_methods={ "pm_saw_mills" "pm_merchant_guilds_building_logging_camp" "pm_no_equipment" "pm_road_carts" "pm_no_hardwood" }
			}
			create_building={
				building="building_coal_mine"
				level=2
				reserves=1
				activate_production_methods={ "pm_atmospheric_engine_pump_building_coal_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" "pm_privately_owned_building_coal_mine" }
			}
			create_building={
				building="building_food_industry"
				level=2
				reserves=1
				activate_production_methods={ "pm_merchant_guilds_building_food_industry" "pm_disabled_canning" "pm_sweeteners" "pm_manual_dough_processing" "pm_disabled_distillery" }
			}
			create_building={
				building="building_paper_mills"
				level=2
				reserves=1
				activate_production_methods={ "pm_pulp_pressing" "pm_automation_disabled" "pm_privately_owned_building_paper_mills" }
			}
			create_building={
				building="building_furniture_manufacturies"
				level=1
				reserves=1
				activate_production_methods={ "pm_lathe" "pm_merchant_guilds_building_furniture_manufacturies" "pm_automation_disabled" "pm_no_luxuries" }
			}
			create_building={
				building="building_textile_mills"
				level=2
				reserves=1
				activate_production_methods={ "pm_dye_workshops" "pm_no_luxury_clothes" "pm_traditional_looms" "pm_merchant_guilds_building_textile_mills" }
			}
			create_building={
				building="building_shipyards"
				level=2
				reserves=1
				activate_production_methods={ "pm_basic_shipbuilding" "pm_merchant_guilds_building_shipyards" "pm_no_military_shipbuilding" }
			}
			create_building={
				building="building_coffee_plantation"
				level=3
				reserves=1
				activate_production_methods={ "default_building_coffee_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_university"
				level=1
				reserves=1
				activate_production_methods={ "pm_scholastic_education" "pm_secular_academia" }
			}
			create_building={
				building="building_arts_academy"
				level=1
				reserves=1
				activate_production_methods={ "pm_traditional_art" }
			}
			create_building={
				building="building_government_administration"
				level=5
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" "pm_secular_bureaucrats" }
			}
			create_building={
				building="building_barracks"
				level=3
				reserves=1
				activate_production_methods={ "pm_no_organization" "pm_mobile_artillery" }
			}
			create_building={
				building="building_naval_base"
				level=5
				reserves=1
				activate_production_methods={ "pm_no_naval_theory" }
			}
			create_building={
				building="building_port"
				level=3
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
			create_building={
				building="building_construction_sector"
				level=3
				reserves=1
				activate_production_methods={ "pm_wooden_buildings" }
			}
		}
	}
	s:STATE_BOEK={
		REGION_STATE:B98={
			create_building={
				building="building_logging_camp"
				level=1
				reserves=1
				activate_production_methods={ "pm_saw_mills" "pm_merchant_guilds_building_logging_camp" "pm_no_equipment" "pm_road_carts" "pm_hardwood" }
			}
			create_building={
				building="building_maize_farm"
				level=2
				reserves=1
				activate_production_methods={ "pm_simple_farming" "pm_no_secondary" "pm_tools" "pm_privately_owned" }
			}
			create_building={
				building="building_livestock_ranch"
				level=1
				reserves=1
				activate_production_methods={ "pm_privately_owned" "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_butchering_tools" }
			}
			create_building={
				building="building_tobacco_plantation"
				level=2
				reserves=1
				activate_production_methods={ "default_building_tobacco_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_iron_mine"
				level=1
				reserves=1
				activate_production_methods={ "pm_atmospheric_engine_pump_building_iron_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" "pm_privately_owned_building_iron_mine" }
			}
			create_building={
				building="building_coal_mine"
				level=1
				reserves=1
				activate_production_methods={ "pm_atmospheric_engine_pump_building_coal_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" "pm_privately_owned_building_coal_mine" }
			}
		}
		REGION_STATE:B89={
			create_building={
				building="building_coffee_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_coffee_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_logging_camp"
				level=1
				reserves=1
				activate_production_methods={ "pm_saw_mills" "pm_merchant_guilds_building_logging_camp" "pm_no_equipment" "pm_road_carts" "pm_no_hardwood" }
			}
			create_building={
				building="building_livestock_ranch"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_open_air_stockyards" }
			}
			create_building={
				building="building_barracks"
				level=1
				reserves=1
				activate_production_methods={ "pm_no_organization" "pm_mobile_artillery" }
			}
		}
	}
	s:STATE_THILVIS={ #add artificer stuff later
		REGION_STATE:B98={
			create_building={
				building="building_great_tower_of_vis"
				level=1
			}
			create_building={
				building="building_logging_camp"
				level=3
				reserves=1
				activate_production_methods={ "pm_saw_mills" "pm_merchant_guilds_building_logging_camp" "pm_no_equipment" "pm_road_carts" "pm_hardwood" }
			}
			create_building={
				building="building_maize_farm"
				level=4
				reserves=1
				activate_production_methods={ "pm_simple_farming" "pm_no_secondary" "pm_tools" "pm_privately_owned" }
			}
			create_building={
				building="building_fishing_wharf"
				level=2
				reserves=1
				activate_production_methods={ "pm_fishing_trawlers" "pm_unrefrigerated" "pm_merchant_guilds_building_fishing_wharf" }
			}
			create_building={
				building="building_coal_mine"
				level=2
				reserves=1
				activate_production_methods={ "pm_atmospheric_engine_pump_building_coal_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" "pm_privately_owned_building_coal_mine" }
			}
			create_building={
				building="building_food_industry"
				level=3
				reserves=1
				activate_production_methods={ "pm_sweeteners" "pm_disabled_canning" "pm_merchant_guilds_building_food_industry" "pm_manual_dough_processing" "pm_pot_stills" }
			}
			create_building={
				building="building_textile_mills"
				level=1
				reserves=1
				activate_production_methods={ "pm_dye_workshops" "pm_no_luxury_clothes" "pm_traditional_looms" "pm_merchant_guilds_building_textile_mills" }
			}
			create_building={
				building="building_furniture_manufacturies"
				level=3
				reserves=1
				activate_production_methods={ "pm_lathe" "pm_merchant_guilds_building_furniture_manufacturies" "pm_automation_disabled" "pm_luxury_furniture" }
			}
			create_building={
				building="building_tooling_workshops"
				level=2
				reserves=1
				activate_production_methods={ "pm_pig_iron" "pm_automation_disabled" "pm_privately_owned_building_tooling_workshops" }
			}
			create_building={
				building="building_arms_industry"
				level=1
				reserves=1
				activate_production_methods={ "pm_muskets" "pm_merchant_guilds_building_arms_industry" "pm_cannons" }
			}
			create_building={
				building="building_university"
				level=1
				reserves=1
				activate_production_methods={ "pm_scholastic_education" }
			}
			create_building={
				building="building_cotton_plantation"
				level=6
				reserves=1
				activate_production_methods={ "default_building_cotton_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_sugar_plantation"
				level=4
				reserves=1
				activate_production_methods={ "default_building_sugar_plantation" "pm_road_carts" "pm_privately_owned" }
			}
			create_building={
				building="building_government_administration"
				level=3
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" }
			}
			create_building={
				building="building_barracks"
				level=5
				reserves=1
				activate_production_methods={ "pm_no_organization" "pm_mobile_artillery" }
			}
			create_building={
				building="building_naval_base"
				level=3
				reserves=1
				activate_production_methods={ "pm_no_naval_theory" }
			}
			create_building={
				building="building_port"
				level=2
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
			create_building={
				building="building_construction_sector"
				level=2
				reserves=1
				activate_production_methods={ "pm_iron_frame_buildings" }
			}
		}
	}
	s:STATE_VALORPOINT={
		REGION_STATE:B98={
			create_building={
				building="building_logging_camp"
				level=1
				reserves=1
				activate_production_methods={ "pm_saw_mills" "pm_merchant_guilds_building_logging_camp" "pm_no_equipment" "pm_road_carts" "pm_no_hardwood" }
			}
			create_building={
				building="building_iron_mine"
				level=1
				reserves=1
				activate_production_methods={ "pm_atmospheric_engine_pump_building_iron_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" "pm_privately_owned_building_iron_mine" }
			}
			create_building={
				building="building_fishing_wharf"
				level=2
				reserves=1
				activate_production_methods={ "pm_fishing_trawlers" "pm_unrefrigerated" "pm_merchant_guilds_building_fishing_wharf" }
			}
			create_building={
				building="building_maize_farm"
				level=2
				reserves=1
				activate_production_methods={ "pm_simple_farming" "pm_no_secondary" "pm_tools" "pm_privately_owned" }
			}
			create_building={
				building="building_livestock_ranch"
				level=3
				reserves=1
				activate_production_methods={ "pm_privately_owned" "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_open_air_stockyards" }
			}
			create_building={
				building="building_food_industry"
				level=1
				reserves=1
				activate_production_methods={ "pm_sweeteners" "pm_disabled_canning" "pm_merchant_guilds_building_food_industry" "pm_manual_dough_processing" "pm_pot_stills" }
			}
			create_building={
				building="building_shipyards"
				level=2
				reserves=1
				activate_production_methods={ "pm_basic_shipbuilding" "pm_merchant_guilds_building_shipyards" "pm_military_shipbuilding_wooden" }
			}
			create_building={
				building="building_coffee_plantation"
				level=2
				reserves=1
				activate_production_methods={ "default_building_coffee_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_sugar_plantation"
				level=2
				reserves=1
				activate_production_methods={ "default_building_sugar_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_barracks"
				level=5
				reserves=1
				activate_production_methods={ "pm_no_organization" "pm_mobile_artillery" }
			}
			create_building={
				building="building_naval_base"
				level=6
				reserves=1
				activate_production_methods={ "pm_no_naval_theory" }
			}
			create_building={
				building="building_port"
				level=2
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
			create_building={
				building="building_construction_sector"
				level=1
				reserves=1
				activate_production_methods={ "pm_wooden_buildings" }
			}
		}
		REGION_STATE:B26={
			create_building={
				building="building_coffee_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_coffee_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
		}
	}
	s:STATE_OKHIBOLI={
		REGION_STATE:B26={
			create_building={
				building="building_maize_farm"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_farming" "pm_no_secondary" "pm_tools_disabled" "pm_privately_owned" }
			}
			create_building={
				building="building_cotton_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_cotton_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_government_administration"
				level=1
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" }
			}
			create_building={
				building="building_barracks"
				level=1
				reserves=1
				activate_production_methods={ "pm_no_organization" "pm_mobile_artillery" }
			}
			create_building={
				building="building_port"
				level=2
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
		}
	}
	s:STATE_SPOORLAND={
		REGION_STATE:B26={
			create_building={
				building="building_livestock_ranch"
				level=2
				reserves=1
				activate_production_methods={ "pm_privately_owned" "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_open_air_stockyards" }
			}
			create_building={
				building="building_logging_camp"
				level=1
				reserves=1
				activate_production_methods={ "pm_saw_mills" "pm_merchant_guilds_building_logging_camp" "pm_no_equipment" "pm_road_carts" "pm_no_hardwood" }
			}
			create_building={
				building="building_tobacco_plantation"
				level=2
				reserves=1
				activate_production_methods={ "default_building_tobacco_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
		}
	}
	s:STATE_KWINELLIANDE={
		REGION_STATE:B18={
			create_building={
				building="building_maize_farm"
				level=4
				reserves=1
				activate_production_methods={ "pm_simple_farming" "pm_vineyards_building_maize_farm" "pm_tools_disabled" "pm_privately_owned" }
			}
			create_building={
				building="building_fishing_wharf"
				level=2
				reserves=1
				activate_production_methods={ "pm_simple_fishing" "pm_unrefrigerated" "pm_merchant_guilds_building_fishing_wharf" }
			}
			create_building={
				building="building_shipyards"
				level=2
				reserves=1
				activate_production_methods={ "pm_basic_shipbuilding" "pm_merchant_guilds_building_shipyards" "pm_military_shipbuilding_wooden" }
			}
			create_building={
				building="building_government_administration"
				level=4
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" }
			}
			create_building={
				building="building_cotton_plantation"
				level=2
				reserves=1
				activate_production_methods={ "default_building_cotton_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_tobacco_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_tobacco_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_dye_plantation"
				level=3
				reserves=1
				activate_production_methods={ "default_building_dye_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_barracks"
				level=2
				reserves=1
				activate_production_methods={ "pm_no_organization" "pm_mobile_artillery" }
			}
			create_building={
				building="building_port"
				level=3
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
			create_building={
				building="building_construction_sector"
				level=1
				reserves=1
				activate_production_methods={ "pm_wooden_buildings" }
			}
		}
	}
	s:STATE_NURTHANNCOST={
		REGION_STATE:B18={
			create_building={
				building="building_maize_farm"
				level=4
				reserves=1
				activate_production_methods={ "pm_simple_farming" "pm_vineyards_building_maize_farm" "pm_tools_disabled" "pm_privately_owned" }
			}
			create_building={
				building="building_fishing_wharf"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_fishing" "pm_unrefrigerated" "pm_merchant_guilds_building_fishing_wharf" }
			}
			create_building={
				building="building_cotton_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_cotton_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_tobacco_plantation"
				level=2
				reserves=1
				activate_production_methods={ "default_building_tobacco_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_dye_plantation"
				level=2
				reserves=1
				activate_production_methods={ "default_building_dye_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_barracks"
				level=1
				reserves=1
				activate_production_methods={ "pm_no_organization" "pm_mobile_artillery" }
			}
			create_building={
				building="building_port"
				level=2
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
		}
	}
	s:STATE_DEATHWOODS={
		REGION_STATE:B18={
			create_building={
				building="building_maize_farm"
				level=2
				reserves=1
				activate_production_methods={ "pm_simple_farming" "pm_vineyards_building_maize_farm" "pm_tools_disabled" "pm_privately_owned" }
			}
			create_building={
				building="building_logging_camp"
				level=3
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_merchant_guilds_building_logging_camp" "pm_no_equipment" "pm_road_carts" "pm_hardwood" }
			}
			create_building={
				building="building_cotton_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_cotton_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_tobacco_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_tobacco_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_dye_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_dye_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_port"
				level=1
				reserves=1
				activate_production_methods={ "pm_anchorage" }
			}
		}
	}
	s:STATE_SORNICANDE={
		REGION_STATE:B18={
			create_building={
				building="building_maize_farm"
				level=3
				reserves=1
				activate_production_methods={ "pm_simple_farming" "pm_no_secondary" "pm_tools_disabled" "pm_privately_owned" }
			}
			create_building={
				building="building_logging_camp"
				level=2
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_merchant_guilds_building_logging_camp" "pm_no_equipment" "pm_road_carts" "pm_hardwood" }
			}
			create_building={
				building="building_iron_mine"
				level=1
				reserves=1
				activate_production_methods={ "pm_atmospheric_engine_pump_building_iron_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" "pm_privately_owned_building_iron_mine" }
			}
			create_building={
				building="building_government_administration"
				level=1
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" }
			}
			create_building={
				building="building_tobacco_plantation"
				level=2
				reserves=1
				activate_production_methods={ "default_building_tobacco_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_dye_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_dye_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
		}
	}
	s:STATE_SOUTH_DALAIRE={
		REGION_STATE:B17={
			create_building={
				building="building_barracks"
				level=5
				reserves=1
				activate_production_methods={ "pm_no_organization" }
			}
		}
		REGION_STATE:B18={
			create_building={
				building="building_maize_farm"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_farming" "pm_no_secondary" "pm_tools_disabled" "pm_privately_owned" }
			}
			create_building={
				building="building_logging_camp"
				level=2
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_merchant_guilds_building_logging_camp" "pm_no_equipment" "pm_road_carts" "pm_hardwood" }
			}
			create_building={
				building="building_sulfur_mine"
				level=1
				reserves=1
				activate_production_methods={ "pm_atmospheric_engine_pump_building_sulfur_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" "pm_privately_owned_building_sulfur_mine" }
			}
			create_building={
				building="building_barracks"
				level=1
				reserves=1
				activate_production_methods={ "pm_no_organization" "pm_mobile_artillery" }
			}
		}
	}
	s:STATE_GRAVEROADS={
		REGION_STATE:B27={
			create_building={
				building="building_maize_farm"
				level=3
				reserves=1
				activate_production_methods={ "pm_simple_farming" "pm_no_secondary" "pm_tools_disabled" "pm_privately_owned" }
			}
			create_building={
				building="building_logging_camp"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_merchant_guilds_building_logging_camp" "pm_no_equipment" "pm_road_carts" "pm_no_hardwood" }
			}
			create_building={
				building="building_fishing_wharf"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_fishing" "pm_unrefrigerated" "pm_merchant_guilds_building_fishing_wharf" }
			}
			create_building={
				building="building_university"
				level=1
				reserves=1
				activate_production_methods={ "pm_scholastic_education" }
			}
			create_building={
				building="building_furniture_manufacturies"
				level=1
				reserves=1
				activate_production_methods={ "pm_handcrafted_furniture" "pm_merchant_guilds_building_furniture_manufacturies" "pm_automation_disabled" "pm_no_luxuries" }
			}
			create_building={
				building="building_coffee_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_coffee_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_government_administration"
				level=1
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" }
			}
			create_building={
				building="building_barracks"
				level=2
				reserves=1
				activate_production_methods={ "pm_no_organization" "pm_mobile_artillery" }
			}
		}
	}
	s:STATE_NERATICA={
		REGION_STATE:B27={
			create_building={
				building="building_maize_farm"
				level=2
				reserves=1
				activate_production_methods={ "pm_simple_farming" "pm_no_secondary" "pm_tools_disabled" "pm_privately_owned" }
			}
			create_building={
				building="building_livestock_ranch"
				level=2
				reserves=1
				activate_production_methods={ "pm_privately_owned" "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_open_air_stockyards" }
			}
			create_building={
				building="building_logging_camp"
				level=2
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_merchant_guilds_building_logging_camp" "pm_no_equipment" "pm_road_carts" "pm_no_hardwood" }
			}
			create_building={
				building="building_fishing_wharf"
				level=2
				reserves=1
				activate_production_methods={ "pm_simple_fishing" "pm_unrefrigerated" "pm_merchant_guilds_building_fishing_wharf" }
			}
			create_building={
				building="building_textile_mills"
				level=2
				reserves=1
				activate_production_methods={ "pm_dye_workshops" "pm_no_luxury_clothes" "pm_traditional_looms" "pm_merchant_guilds_building_textile_mills" }
			}
			create_building={
				building="building_paper_mills"
				level=1
				reserves=1
				activate_production_methods={ "pm_sulfite_pulping" "pm_automation_disabled" "pm_privately_owned_building_paper_mills" }
			}
			create_building={
				building="building_sugar_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_sugar_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_government_administration"
				level=2
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" }
			}
			create_building={
				building="building_barracks"
				level=8
				reserves=1
				activate_production_methods={ "pm_no_organization" "pm_mobile_artillery" }
			}
			create_building={
				building="building_port"
				level=2
				reserves=1
				activate_production_methods={ "pm_anchorage" }
			}
			create_building={
				building="building_construction_sector"
				level=1
				reserves=1
				activate_production_methods={ "pm_wooden_buildings" }
			}
		}
	}
	s:STATE_DARKGROVES={
		REGION_STATE:B27={
			create_building={
				building="building_maize_farm"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_farming" "pm_no_secondary" "pm_tools_disabled" "pm_privately_owned" }
			}
			create_building={
				building="building_dye_plantation"
				level=2
				reserves=1
				activate_production_methods={ "default_building_dye_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_sugar_plantation"
				level=2
				reserves=1
				activate_production_methods={ "default_building_sugar_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_barracks"
				level=4
				reserves=1
				activate_production_methods={ "pm_no_organization" "pm_mobile_artillery" }
			}
		}
	}
	s:STATE_MORDUN={
		REGION_STATE:B27={
			create_building={
				building="building_iron_mine"
				level=1
				reserves=1
				activate_production_methods={ "pm_atmospheric_engine_pump_building_iron_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" "pm_privately_owned_building_iron_mine" }
			}
			create_building={
				building="building_banana_plantation"
				level=3
				reserves=1
				activate_production_methods={ "default_building_banana_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_sugar_plantation"
				level=2
				reserves=1
				activate_production_methods={ "default_building_sugar_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_port"
				level=1
				reserves=1
				activate_production_methods={ "pm_anchorage" }
			}
		}
	}
}