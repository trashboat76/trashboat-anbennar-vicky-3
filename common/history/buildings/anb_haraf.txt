﻿BUILDINGS={
	s:STATE_STEEL_BAY = {
		region_state:B05 = {
			create_building={
				building="building_government_administration"
				level=2
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" "pm_secular_bureaucrats" }
			}
			create_building={
				building="building_tooling_workshops"
				level=1
				reserves=1
				activate_production_methods={ "pm_steel" "pm_automation_disabled" "pm_privately_building_tooling_workshops" }
			}
			create_building={
				building="building_steel_mills"
				level=1
				reserves=1
				activate_production_methods={ "pm_blister_steel_process" "pm_automation_disabled" "pm_privately_owned_industry" }
			}
			create_building={
				building="building_arms_industry"
				level=3
				reserves=1
				activate_production_methods={ "pm_muskets" "pm_merchant_guilds_building_arms_industry" "pm_cannons" }
			}
			create_building={
				building="building_shipyards"
				level=1
				reserves=1
				activate_production_methods={ "pm_basic_shipbuilding" "pm_merchant_guilds_building_shipyards" "pm_military_shipbuilding_wooden" }
			}
			create_building={
				building="building_university"
				level=1
				reserves=1
				activate_production_methods={ "pm_scholastic_education" "pm_secular_academia" }
			}
			create_building={
				building="building_barracks"
				level=10
				reserves=1
				activate_production_methods={ "pm_no_organization" "pm_mobile_artillery" }
			}
			create_building={
				building="building_naval_base"
				level=10
				reserves=1
				activate_production_methods={ "pm_no_naval_theory" }
			}
			create_building={
				building="building_port"
				level=1
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
			create_building={
				building="building_rice_farm"
				level=2
				reserves=1
				activate_production_methods={ "pm_simple_farming_building_rice_farm" "pm_privately_owned" "pm_tools_building_rice_farm" "pm_fig_orchards_building_rice_farm" }
			}
			create_building={
				building="building_livestock_ranch"
				level=1
				reserves=1
				activate_production_methods={ "pm_privately_owned" "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_slaughterhouses" }
			}
			create_building={
				building="building_coffee_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_coffee_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_coal_mine"
				level=2
				reserves=1
				activate_production_methods={ "pm_atmospheric_engine_pump_building_coal_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" "pm_privately_owned_building_coal_mine" }
			}
			create_building={
				building="building_iron_mine"
				level=2
				reserves=1
				activate_production_methods={ "pm_atmospheric_engine_pump_building_iron_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" "pm_privately_owned_building_iron_mine" }
			}
		}
	}
	s:STATE_GREENHILL = {
		region_state:B05 = {
			create_building={
				building="building_government_administration"
				level=1
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" "pm_secular_bureaucrats" }
			}
			create_building={
				building="building_rice_farm"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_farming_building_rice_farm" "pm_privately_owned" "pm_tools_building_rice_farm" "pm_fig_orchards_building_rice_farm" }
			}
			create_building={
				building="building_logging_camp"
				level=2
				reserves=1
				activate_production_methods={ "pm_saw_mills" "pm_merchant_guilds_building_logging_camp" "pm_saw_mills" "pm_road_carts" "pm_hardwood" }
			}
			create_building={
				building="building_livestock_ranch"
				level=1
				reserves=1
				activate_production_methods={ "pm_privately_owned" "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_slaughterhouses" }
			}
			create_building={
				building="building_iron_mine"
				level=1
				reserves=1
				activate_production_methods={ "pm_atmospheric_engine_pump_building_iron_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" "pm_privately_owned_building_iron_mine" }
			}
			create_building={
				building="building_barracks"
				level=5
				reserves=1
				activate_production_methods={ "pm_no_organization" "pm_mobile_artillery" }
			}
			create_building={
				building="building_naval_base"
				level=2
				reserves=1
				activate_production_methods={ "pm_no_naval_theory" }
			}
		}
	}
	s:STATE_RYAIL = {
		region_state:B05 = {
			create_building={
				building="building_tobacco_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_tobacco_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_rice_farm"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_farming_building_rice_farm" "pm_privately_owned" "pm_tools_disabled" "pm_fig_orchards_building_rice_farm" }
			}
			create_building={
				building="building_port"
				level=1
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
		}
	}
	s:STATE_SCATTERED_ISLANDS = {
		region_state:B05 = {
			create_building={
				building="building_banana_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_banana_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
		}
	}
}