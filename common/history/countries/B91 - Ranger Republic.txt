﻿COUNTRIES = {
	c:B91 = {
		effect_starting_technology_tier_3_tech = yes
		add_technology_researched = napoleonic_warfare
		add_technology_researched = empiricism

		effect_starting_politics_conservative = yes
		
		activate_law = law_type:law_presidential_republic
		activate_law = law_type:law_oligarchy
		activate_law = law_type:law_national_supremacy
		activate_law = law_type:law_state_religion
		activate_law = law_type:law_appointed_bureaucrats
		activate_law = law_type:law_professional_army
		# No home affairs
		activate_law = law_type:law_interventionism
		activate_law = law_type:law_protectionism
		activate_law = law_type:law_per_capita_based_taxation
		activate_law = law_type:law_frontier_colonization
		activate_law = law_type:law_no_health_system
		activate_law = law_type:law_no_schools
		
		activate_law = law_type:law_censorship
		activate_law = law_type:law_homesteading
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_women_own_property
		# No social security
		activate_law = law_type:law_slavery_banned
		
		activate_law = law_type:law_non_monstrous_only
		activate_law = law_type:law_artifice_encouraged

		set_institution_investment_level = {
			institution = institution_police #Rangers, as per the name
			level = 2
		}
	}
}