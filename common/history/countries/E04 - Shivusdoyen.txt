﻿COUNTRIES = {
	c:E04 = {
		effect_starting_technology_tier_4_tech = yes
		
		#Triunic Liberalism
		add_technology_researched = academia
		add_technology_researched = urban_planning
		add_technology_researched = centralization
		add_technology_researched = law_enforcement
		add_technology_researched = currency_standards
		add_technology_researched = colonization
		add_technology_researched = romanticism
		add_technology_researched = medical_degrees
		add_technology_researched = empiricism
		
		
		activate_law = law_type:law_presidential_republic
		activate_law = law_type:law_oligarchy
		activate_law = law_type:law_racial_segregation
		activate_law = law_type:law_freedom_of_conscience
		activate_law = law_type:law_hereditary_bureaucrats
		activate_law = law_type:law_professional_army
		activate_law = law_type:law_no_home_affairs

		activate_law = law_type:law_traditionalism
		activate_law = law_type:law_mercantilism
		activate_law = law_type:law_land_based_taxation
		activate_law = law_type:law_tenant_farmers
		activate_law = law_type:law_no_colonial_affairs
		activate_law = law_type:law_no_police
		activate_law = law_type:law_religious_schools
		activate_law = law_type:law_no_health_system

		activate_law = law_type:law_right_of_assembly
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_no_womens_rights
		activate_law = law_type:law_no_social_security
		activate_law = law_type:law_migration_controls
		activate_law = law_type:law_slavery_banned
		
		activate_law = law_type:law_same_race_only
		activate_law = law_type:law_dark_arts_banned
		activate_law = law_type:law_amoral_artifice_banned
		activate_law = law_type:law_mundane_production

	}
}