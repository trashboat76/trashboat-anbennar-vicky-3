﻿COUNTRIES = {
	c:C11 = {
		effect_starting_technology_tier_4_tech = yes
		
		effect_starting_politics_traditional = yes
		
		# Laws 
		activate_law = law_type:law_presidential_republic
		activate_law = law_type:law_autocracy
		activate_law = law_type:law_national_supremacy
		activate_law = law_type:law_state_religion
		activate_law = law_type:law_hereditary_bureaucrats
		activate_law = law_type:law_peasant_levies
		activate_law = law_type:law_no_home_affairs
		
		activate_law = law_type:law_traditionalism
		activate_law = law_type:law_land_based_taxation
		activate_law = law_type:law_tenant_farmers
		
		activate_law = law_type:law_censorship
		activate_law = law_type:law_migration_controls
		activate_law = law_type:law_slavery_banned
		
		activate_law = law_type:law_same_race_and_humans
		activate_law = law_type:law_dark_arts_banned
		activate_law = law_type:law_mundane_production
	}
}