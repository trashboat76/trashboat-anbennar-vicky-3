﻿COUNTRIES = {
	c:A18 = {
		effect_starting_technology_tier_1_tech = yes
		
		add_technology_researched = nitroglycerin
		
		activate_law = law_type:law_parliamentary_republic
		activate_law = law_type:law_wealth_voting
		activate_law = law_type:law_cultural_exclusion
		activate_law = law_type:law_freedom_of_conscience
		activate_law = law_type:law_appointed_bureaucrats		
		activate_law = law_type:law_peasant_levies
		# No home affairs
		
		activate_law = law_type:law_laissez_faire
		activate_law = law_type:law_free_trade
		activate_law = law_type:law_per_capita_based_taxation
		activate_law = law_type:law_colonial_exploitation
		activate_law = law_type:law_local_police
		activate_law = law_type:law_tenant_farmers
		
		# No healthcare
		
		activate_law = law_type:law_censorship
		#activate_law = law_type:law_serfdom_banned
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_women_own_property
		activate_law = law_type:law_poor_laws
		activate_law = law_type:law_slavery_banned
		
		activate_law = law_type:law_same_race_and_humans
		activate_law = law_type:law_dark_arts_banned
		activate_law = law_type:law_amoral_artifice_banned
		activate_law = law_type:law_artifice_encouraged	

		ig:ig_armed_forces = {
			add_ruling_interest_group = yes
		}
		ig:ig_industrialists = {
			add_ruling_interest_group = yes
		}

	}
}