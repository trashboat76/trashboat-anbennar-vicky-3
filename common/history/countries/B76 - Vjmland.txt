﻿COUNTRIES = {
	c:B76= {
		effect_starting_technology_tier_3_tech = yes

		effect_starting_politics_conservative = yes
		
		activate_law = law_type:law_presidential_republic
		activate_law = law_type:law_landed_voting
		activate_law = law_type:law_national_supremacy
		activate_law = law_type:law_state_religion
		activate_law = law_type:law_appointed_bureaucrats
		activate_law = law_type:law_national_militia
		activate_law = law_type:law_agrarianism
		activate_law = law_type:law_mercantilism
		activate_law = law_type:law_per_capita_based_taxation
		activate_law = law_type:law_frontier_colonization
		activate_law = law_type:law_no_schools
		activate_law = law_type:law_no_health_system
		activate_law = law_type:law_no_police
		activate_law = law_type:law_censorship
		activate_law = law_type:law_tenant_farmers
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_women_own_property
		activate_law = law_type:law_legacy_slavery
		activate_law = law_type:law_artifice_encouraged
	}
}