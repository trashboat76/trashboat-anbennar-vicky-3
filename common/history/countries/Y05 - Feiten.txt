﻿COUNTRIES = {
	c:Y05 = {
		effect_starting_technology_tier_4_tech = yes
		add_technology_researched = urban_planning
		add_technology_researched = sericulture
		add_technology_researched = academia
		add_technology_researched = law_enforcement
		
		effect_starting_politics_traditional = yes
		activate_law = law_type:law_parliamentary_republic
		activate_law = law_type:law_wealth_voting
		activate_law = law_type:law_freedom_of_conscience
		activate_law = law_type:law_elected_bureaucrats
		activate_law = law_type:law_national_militia

		activate_law = law_type:law_tenant_farmers

		activate_law = law_type:law_right_of_assembly
		activate_law = law_type:law_women_own_property
		activate_law = law_type:law_no_migration_controls
	}
}