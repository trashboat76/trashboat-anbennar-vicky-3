﻿COUNTRIES = {
	c:L09 = {
		effect_starting_technology_baashidi_tech = yes

		effect_starting_politics_conservative = yes
		
		activate_law = law_type:law_presidential_republic
		activate_law = law_type:law_no_police
		activate_law = law_type:law_appointed_bureaucrats
		activate_law = law_type:law_homesteading
		activate_law = law_type:law_no_womens_rights
		activate_law = law_type:law_landed_voting
		activate_law = law_type:law_racial_segregation
	}
}