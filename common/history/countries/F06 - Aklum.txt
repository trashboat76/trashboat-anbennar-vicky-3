﻿COUNTRIES = {
	c:F06 ?= {
		effect_starting_technology_tier_2_tech = yes
		
		activate_law = law_type:law_parliamentary_republic
		activate_law = law_type:law_oligarchy
		activate_law = law_type:law_cultural_exclusion
		activate_law = law_type:law_state_religion
		activate_law = law_type:law_appointed_bureaucrats #Elected bureaucrats not allowed without elections
		
		activate_law = law_type:law_land_based_taxation
		
		activate_law = law_type:law_right_of_assembly
		activate_law = law_type:law_women_own_property
		
		activate_law = law_type:law_non_monstrous_only
		activate_law = law_type:law_dark_arts_banned
		activate_law = law_type:law_amoral_artifice_banned
		activate_law = law_type:law_mundane_production
	}
}