﻿CHARACTERS = {
	c:B05 = {
		create_character = {
			first_name = "Dalyon"
			last_name = "of_Vanbury"
			historical = yes
			ruler = yes
			age = 42
			interest_group = ig_industrialists
			ideology = ideology_royalist
			traits = {
				ambitious innovative
			}
		}
	}
}
