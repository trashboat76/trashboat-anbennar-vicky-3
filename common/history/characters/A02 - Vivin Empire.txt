﻿CHARACTERS = {
	c:A02 = {
		create_character = {
			first_name = Petre
			last_name = sil_Vivin
			historical = yes
			ruler = yes
			noble = yes
			#age = 73
			birth_date = 1763.9.15
			interest_group = ig_landowners
			ideology = ideology_royalist
			traits = {
				imperious bandit
			}
		}

		create_character = {
			first_name = Petre_Nicolas	#Nicolae is his grandfather on Silmuna side. Petre's grandson and half silmuna btw
			last_name = sil_Vivin
			historical = yes
			heir = yes
			noble = yes
			birth_date = 1814.6.27
			interest_group = ig_petty_bourgeoisie
			ideology = ideology_moderate
			traits = {
				reserved basic_political_operator
			}
		}
	}
}
