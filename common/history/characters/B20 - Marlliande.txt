﻿CHARACTERS = {
	c:B20 ?= {
		create_character = { #Guy who stood up to the other vampire clans and stayed loyal to the Concord
			first_name = "Willam"
			last_name = Dainuich
			historical = yes
			ruler = yes
			noble = yes
			age = 41
			interest_group = ig_landowners
			ig_leader = yes
			ideology = ideology_jingoist_leader
			traits = {
				brave experienced_diplomat
			}
		}
	}
}
