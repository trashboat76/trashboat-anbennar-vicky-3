﻿CHARACTERS = {
	c:A01 = {
		create_character = {
			first_name = Artur_Laurens
			last_name = Silmuna
			historical = yes
			ruler = yes
			age = 10
			birth_date = 1826.2.4
			interest_group = ig_intelligentsia
			ideology = ideology_reformer
			dna = dna_artur_laurens_silmuna
			traits = {
				charismatic demagogue
			}
		}

		#book dude
		create_character = {
			first_name = Rean
			last_name = sil_Tirufeld
			historical = yes
			age = 49	#born 1786
			interest_group = ig_intelligentsia
			ig_leader = yes
			ideology = ideology_constitutionalist
			traits = {
				tactful expert_political_operator
			}
		}

		create_character = {
			is_general = yes
			first_name = Artura
			last_name = Mergirn
			historical = yes
			female = yes
			age = 55
			interest_group = ig_armed_forces
			ig_leader = yes
			ideology = ideology_royalist
			hq = region_eastern_dameshead
			commander_rank = commander_rank_4
			traits = {
				imperious traditionalist_commander
			}
		}
	}
}
