﻿
building_st_torrieths_bastion_of_the_god_fragment = {
	building_group = bg_monuments
	texture = "gfx/interface/icons/building_icons/vatican_city.dds"
	expandable = no
	buildable = no
	downsizeable = no
	unique = yes
	locator = "vatican_city_locator"
	
	entity_not_constructed = { }
	entity_under_construction = { "building_construction_3x3_entity" }
	entity_constructed = { "wonder_vatican_city_01_entity"}
	
	city_gfx_interactions = {
		clear_size_area = yes
		size = 6
	
	}
	
	production_method_groups = {
		pmg_base_building_st_torrieths_bastion_of_the_god_fragment
	}
	
	possible = {
		error_check = {
			severity = invalid
			this = {
				state_region = s:STATE_PASHAINE
			}
		}
	}
	
	required_construction = construction_cost_monument
}

building_great_tower_of_vis = {
	building_group = bg_monuments
	texture = "gfx/interface/icons/building_icons/big_ben.dds"
	expandable = no
	downsizeable = no
	unique = yes
	locator = "big_ben_locator"
	
	entity_not_constructed = { }
	entity_under_construction = { "building_construction_2x2_entity" }
	entity_constructed = { "wonder_big_ben_01_entity"}
	
	city_gfx_interactions = {
		clear_size_area = yes
		size = 2
	}
	
	production_method_groups = {
		pmg_base_building_great_tower_of_vis
	}
	
	required_construction = construction_cost_monument
	
	possible = {
		error_check = {
			severity = invalid
			this = {
				state_region = s:STATE_THILVIS
			}
		}
	}
	
	ai_value = 10000
}