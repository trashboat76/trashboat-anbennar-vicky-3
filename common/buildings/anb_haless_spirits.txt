﻿

building_haless_spirit_presence = {
	building_group = bg_haless_spirits
	# texture = "gfx/interface/icons/building_icons/haless_spirit_presence.dds"
	city_type = wood
	levels_per_mesh = 5
	buildable = no
	expandable = no
	downsizeable = no

	production_method_groups = {
		pmg_spirit_presence_attitude
	}
	
}

building_haless_high_temple = {
	building_group = bg_haless_temples
	# texture = "gfx/interface/icons/building_icons/haless_spirits_high_temple.dds"
	city_type = city
	levels_per_mesh = 5
	buildable = no
	expandable = no
	downsizeable = no

	production_method_groups = {
		pmg_high_temples_management
	}
	
}

building_spirit_extractors = {
	building_group = bg_haless_extractors
	# texture = "gfx/interface/icons/building_icons/spirit_extractors.dds"
	city_type = wood
	required_construction = construction_cost_low
	terrain_manipulator = forestry
	levels_per_mesh = 5
	
	unlocking_technologies = {
		navigation
	}

	production_method_groups = {
		pmg_base_building_logging_camp
		pmg_hardwood
		pmg_equipment
		pmg_transportation_building_logging_camp
		pmg_ownership_capital_building_logging_camp
	}
}
