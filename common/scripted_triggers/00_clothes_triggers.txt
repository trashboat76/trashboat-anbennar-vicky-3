﻿#Anbennar
#Added a bunch of equivalencies, left the bespoke ones like british empire etc for now

elven_clothes_pop_trigger = {
	exists = scope:pop
	scope:pop = {
		#Anbennar
		culture = {
			OR = {
				has_discrimination_trait = elven_race_heritage
			}
		}   
	}
}

elven_clothes_trigger = {
	exists = scope:character
	scope:character = {
		culture = {
			has_discrimination_trait = elven_race_heritage
		}
	}
}

british_empire_clothes_trigger = {
	exists = scope:character
	scope:character = {
		exists = c:A00
		exists = home_country
		home_country = {
			OR = {
				AND = {
					c:A00 = this
					is_subject = no # preventing uniform conflicts if A00 becomes a subject of another country with puppet uniforms
				}
				is_subject_of = c:A00
			}		
		}
	}
}

british_empire_clothes_pop_trigger = {
	exists = scope:pop
	scope:pop = {
		exists = c:A00
		home_country = {
			OR = {
				AND = {
					c:A00 = this
					is_subject = no # preventing uniform conflicts if A00 becomes a subject of another country with puppet uniforms
				}
				is_subject_of = c:A00
			}		
		}
	}
}

british_empire_clothes_p_trigger = {
	exists = scope:pop
	scope:pop = {
		exists = c:A00
		home_country = {
			OR = {
				AND = {
					c:A00 = this
					is_subject = yes # preventing uniform conflicts if A00 becomes a subject of another country with puppet uniforms
				}
				is_subject_of = c:A00
			}		
		}
	}
}

french_empire_clothes_trigger = {
	exists = scope:character
	scope:character = {
		exists = c:A00
		exists = home_country
		home_country = {
			OR = {
				AND = {
					c:A00 = this
					is_subject = no # preventing uniform conflicts if A00 becomes a subject of another country with puppet uniforms
				}
				is_subject_of = c:A00
			}		
		}
	}
}

french_empire_clothes_pop_trigger = {	#invalidated for anbennar
	exists = scope:pop
	scope:pop = {
		exists = c:A00
		home_country = {
			OR = {
				AND = {
					c:A00 = this
					is_subject = no # preventing uniform conflicts if A00 becomes a subject of another country with puppet uniforms
				}
				is_subject_of = c:A00
			}		
		}
	}
}

german_clothes_trigger = {
	exists = scope:character
	scope:character = {
		exists = home_country
		home_country = {
			is_subject = no
			OR = {
				#country_has_primary_culture = cu:north_german
				#country_has_primary_culture = cu:south_german
				country_has_primary_culture = cu:testorian
			}	
		}
	}
}

german_clothes_pop_trigger = {
	exists = scope:pop
	scope:pop = {
		home_country = {
			is_subject = no
			OR = {
				#country_has_primary_culture = cu:north_german
				#country_has_primary_culture = cu:south_german
				country_has_primary_culture = cu:testorian
			}	
		}
	}
}


native_american_clothes_trigger = {
	exists = scope:character
	scope:character = {
		#Anbennar
		culture = {
			has_discrimination_trait = noruinic

			#has_discrimination_trait = indigenous_american_heritage
		}
	}
}

native_north_american_clothes_trigger = {
	exists = scope:character
	scope:character = {
		culture = {
			has_discrimination_trait = noruinic

			#has_discrimination_trait = north_american_group
		}
	}
}

muslim_clothes_pop_trigger = {
     exists = scope:pop
  	scope:pop = {
		#Anbennar
		religion = {
			has_discrimination_trait = sun_cult

			#has_discrimination_trait = muslim
		}
	}
}

jewish_clothes_pop_trigger = {
     exists = scope:pop
  	scope:pop = {
		#Anbennar
		religion = {
			has_discrimination_trait = cannorian_religion	#trying this out lol

			#has_discrimination_trait = jewish
		}
	}
}

hindu_clothes_pop_trigger = {
	exists = scope:pop
	scope:pop = {
		#Anbennar
		religion = { has_discrimination_trait = raheni_religion }
		#religion = rel:high_philosophy

		#religion = rel:hindu
	}
}

african_diaspora_clothes_trigger = {
	exists = scope:character
	scope:character = {
		OR = {
			culture = cu:afro_caribbean
			culture = cu:afro_antillean
			culture = cu:afro_caribeno
			culture = cu:afro_american
			culture = cu:afro_brazilian
		}
	}
}

african_diaspora_pop_clothes_trigger = {
	exists = scope:pop
	scope:pop = {
		OR = {
			culture = cu:afro_caribbean
			culture = cu:afro_antillean
			culture = cu:afro_caribeno
			culture = cu:afro_american
			culture = cu:afro_brazilian
		}
	}
}

japanese_imperial_clothes_trigger = {
	exists = scope:character
	#Anbennar - i mean kinda changed but we'll see
	scope:character = {
		is_ruler = yes
		exists = home_country
     	home_country = {
     		has_law = law_type:law_monarchy
     	}
		OR = {
			AND = {
				culture = {
					has_discrimination_trait = command_heritage
					#has_discrimination_trait = yamato
				}
				#OR = {
				#	NOT = {
				#		exists = c:JAP
				#	}
				#	AND = {
				#		exists = c:JAP
				#		NOT = { owner = c:JAP }
				#	}
				#}
			}
			#AND = {
			#	exists = c:JAP
			#	OWNER = {
			#		this = c:JAP
			#	}
			#}
		}
	}
}

chinese_imperial_clothes_trigger = {
	exists = scope:character
	scope:character = {
		#Anbennar
		is_ruler = yes
		exists = home_country
     	home_country = {
     		has_law = law_type:law_monarchy
     	}
		OR = {
			culture = {
				has_discrimination_trait = yan
				#has_discrimination_trait = han_chinese
			}
			AND = {
				exists = c:A00	#Anbennar TODO: change this to united yanshen country
				OWNER = c:A00
			}
		}
	}
}

chinese_manchu_queue_hairstyle_character_trigger = { # hairsyle for Manchu characters not living living under discriminatory Han rule, and Han characters living under discriminatory Manchu rule
	exists = scope:character
	scope:character = {
		exists = home_country
		OR = {
			AND = {
				home_country = { 
					ruler = { 
						has_culture = cu:testorian 
					}
					NOT = { 
						has_law = law_type:law_multicultural
					}
				}
				culture = { 
					has_discrimination_trait = han_chinese 
				}
			}
			AND = {
				NOT = {
					home_country = { 
						ruler = { 
							culture = { 
								has_discrimination_trait = han_chinese 
							}
						}
						cu:testorian = {
							NOT = { 
								culture_is_discriminated_in = scope:character.home_country 
							}
						}
					}
				}
				culture = cu:testorian
			}
		}
	}
}

chinese_manchu_queue_hairstyle_pops_trigger = { # hairsyle for Manchu pops not living living under discriminatory Han rule, and Han pops living under discriminatory Manchu rule
	exists = scope:pop
	scope:pop = {
		OR = {
			AND = {
				home_country = { 
					ruler = { 
						has_culture = cu:testorian 
					}
					NOT = { 
						has_law = law_type:law_multicultural
					}
				}
				culture = { 
					has_discrimination_trait = han_chinese 
				}
			}
			AND = {
				NOT = {
					home_country = { 
						ruler = { 
							culture = { 
								has_discrimination_trait = han_chinese 
							}
						}
						cu:testorian = {
							NOT = { 
								culture_is_discriminated_in = scope:pop.home_country 
							}
						}
					}
				}
				culture = cu:testorian
			}
		}
	}
}

chinese_court_clothes_trigger = {
	exists = scope:character
	scope:character = {
		#Anbennar, dunno if this is right but ye
		NOT = {
			AND = {
				exists = home_country
				is_ruler = yes
				culture = {
					has_discrimination_trait = southeast_halessi_heritage
					#has_discrimination_trait = tai
					#has_discrimination_trait = southeast_asian_heritage
				}
		     	home_country = {
		     		OR = {
			     		has_law = law_type:law_monarchy
			     		has_law = law_type:law_theocracy
			     	}
		     	}			
			}
		}
		culture = {
			NOT = { this = cu:kalmyk }
			OR = {
				has_discrimination_trait = yan

				#has_discrimination_trait = sinosphere
				#has_discrimination_trait = han_chinese
				#has_discrimination_trait = tibetan_language
				#has_discrimination_trait = mongol_language
				#has_discrimination_trait = tai
				#has_discrimination_trait = hmong_mien_language
			}
		}
	}	
}

chinese_court_clothes_pop_trigger = {
	exists = scope:pop
	scope:pop = {
		#Anbennar
		culture = {
			OR = {
				has_discrimination_trait = yan

				#has_discrimination_trait = sinosphere
				#has_discrimination_trait = han_chinese
				#has_discrimination_trait = tibetan_language
				#has_discrimination_trait = mongol_language
				#has_discrimination_trait = tai
				#has_discrimination_trait = hmong_mien_language
			}
		}
	}	
}


african_clothes_trigger = {
	exists = scope:character
	scope:character = {
		#Anbennar
		culture = {
			OR = {
				has_discrimination_trait = sarhaly_heritage	#may need to be broken up eventually

				#has_discrimination_trait = central_african_culture_group
				#has_discrimination_trait = east_african_culture_group
				#has_discrimination_trait = west_african_culture_group
				#has_discrimination_trait = southern_african_culture_group
				#has_discrimination_trait = malagasy_language
			}
		}
	}     
}

arabic_fez_trigger = {
	exists = scope:character
	scope:character = {
		exists = home_country
		OR = {
			home_country = {
				OR = {
					is_country_type = recognized
					is_country_type = colonial
				}	
			}
			home_country = {
				top_overlord = {
					OR = {
						is_country_type = recognized
						is_country_type = colonial
					}	
				}
			}
			home_country = {
				exists = c:A00	#Anbennar TODO - change this ti kheteratan 
				c:A00 = this
			}
		}
	}       
}

arabic_military_fez_pop_trigger = {
	exists = scope:pop
	scope:pop = {
		AND = {
			OR = {
				home_country = {
					exists = c:A00
					c:A00 = this
					c:A00 = { has_law = law_type:law_monarchy }
				}
				home_country = {
					exists = c:A00
					c:A00 = this
					c:A00 = { has_law = law_type:law_monarchy }
				}
				home_country = {
					exists = c:A00
					c:A00 = this
					c:A00 = { has_law = law_type:law_monarchy }
				}
				home_country = {
					exists = c:A00
					c:A00 = this
					c:A00 = { has_law = law_type:law_monarchy }
				}
			}
			OR = {
				culture = {
					OR = {
						has_discrimination_trait = middle_eastern_heritage
						has_discrimination_trait = central_asian_heritage
					}
				}
				AND = { # Albanian characters serving either the Ottomans or Egyptians should wear that country's clothing.
					culture = cu:albanian
					OR = {
						home_country = {
							exists = c:A00
							c:A00 = this
							c:A00 = { has_law = law_type:law_monarchy }
						}
						home_country = {
							exists = c:A00
							c:A00 = this
							c:A00 = { has_law = law_type:law_monarchy }
						}
					}
				}
			}
		}
	}       
}

arabic_clothes_trigger = {
	exists = scope:character
	scope:character = {
		#Anbennar
		OR = {
			culture = {
				OR = {
					has_discrimination_trait = bulwari_heritage
					has_discrimination_trait = divenori_heritage	#change this long term
					#has_discrimination_trait = middle_eastern_heritage
					#has_discrimination_trait = central_asian_heritage #Currently no point having a different check
				}
			}
		}
	}        
}

ethiopian_clothes_trigger = {
	exists = scope:character
	scope:character = {
		culture = {
			has_discrimination_trait = ethiopian	#make this melakmengi
		}
	}
}

european_clothes_trigger = {
	exists = scope:character
	scope:character = {
		culture = {	#TODO add the japanese meiji western clothes ver for dudes like LF
			has_discrimination_trait = cannorian_heritage	#Anbennar
			#has_discrimination_trait = european_heritage
		}
	}
}

indian_clothes_trigger = {
	exists = scope:character
	scope:character = {
		#Anbennar
		OR = {
			culture = {
				has_discrimination_trait = raheni_heritage
				#has_discrimination_trait = south_asian_heritage
			}
			#religion = rel:sikh
		}
	}
}

siamese_clothes_trigger = { #Pretty sure this is actual burmese
	exists = scope:character
	scope:character = {
		culture = {
			has_discrimination_trait = southeast_asian_heritage
		}
	}
}

american_clothes_trigger = {
	exists = scope:character
	scope:character = {
		exists = home_country
		home_country = {
			exists = c:A00
			c:A00 = this
			is_subject = no
		}
	}
}

american_clothes_pop_trigger = {
	exists = scope:pop
	scope:pop = {
		home_country = {
			exists = c:A00
			c:A00 = this
			is_subject = no
		}
	}
}

politician_clothes_trigger = {
	exists = scope:character
	scope:character = {
		AND = {
			has_role = politician
			is_ruler = no
		}
	}
}

recognized_country_trigger = {
	exists = scope:character
	scope:character = {
		exists = home_country
		OR = {
			home_country = {
				OR = {
					is_country_type = recognized
					is_country_type = colonial
				}	
			}
			home_country = {
				top_overlord = {
					OR = {
						is_country_type = recognized
						is_country_type = colonial
					}	
				}
			}
		}
	}       
}

recognized_country_pop_trigger = {
	exists = scope:pop
	scope:pop = {
		OR = {
			home_country = {
				OR = {
					is_country_type = recognized
					is_country_type = colonial
				}	
			}
			home_country = {
				top_overlord = {
					OR = {
						is_country_type = recognized
						is_country_type = colonial
					}	
				}
			}
		}
	}       
}

christian_clothes_trigger = {
  	exists = scope:character
	scope:character = {
		#Anbennar
		religion = rel:ravelian
			
		#has_discrimination_trait = christian
	}
}

devout_clothes_trigger = {
	exists = scope:character
	scope:character = {
		is_interest_group_type = ig_devout
	}
}

armed_forces_clothes_trigger = {
	exists = scope:character
	scope:character = {
		is_interest_group_type = ig_armed_forces
	}
}

ruler_clothes_trigger = {
	exists = scope:character
	scope:character = {
		is_ruler = yes
	}
}

military_clothes_trigger = {
	exists = scope:character
	scope:character = {
		OR = {
			has_role = general
			has_role = admiral
		}
	}
}

monarchy_clothes_trigger = {
	exists = scope:character
	scope:character = {
		exists = home_country
		OR = {
	     	is_ruler = yes
	     	is_heir = yes
	     }
     	home_country = {
     		has_law = law_type:law_monarchy
     	}
	}
}

republic_clothes_trigger = {
	exists = scope:character
	scope:character = {
		exists = home_country
		is_ruler = yes
		home_country = {
			NOT = { has_law = law_type:law_monarchy}
		}
	}
}

overweight_trigger = {
	exists = scope:character
	scope:character = {
     	has_trait = expensive_tastes
     }
}

#Pop 

east_asian_clothes_pop_trigger = {
	exists = scope:pop
	scope:pop = {
		#Anbennar
		NOR = { 
			culture = cu:yakut 
			culture = cu:siberian
		}
		culture = {
			OR = {
				has_discrimination_trait = northeast_halessi_heritage
				has_discrimination_trait = southeast_halessi_heritage
				#has_discrimination_trait = east_asian_heritage
				#has_discrimination_trait = north_asian_heritage
			}
		}   
	}
}

central_asian_clothes_pop_trigger = {
	exists = scope:pop
	scope:pop = {
		#Anbennar - maybe fp?
		culture = {
			OR = {
				has_discrimination_trait = fp_heritage
				#has_discrimination_trait = central_asian_heritage

			}
		}   
	}
}

european_clothes_pop_trigger = {
	exists = scope:pop
	scope:pop = {
		#Anbennar
		culture = {
			has_discrimination_trait = cannorian_heritage

			#has_discrimination_trait = european_heritage
		}
	}
}

arabic_clothes_pop_trigger = {
	exists = scope:pop
	scope:pop = {
		culture = {
			OR = {
				has_discrimination_trait = bulwari_heritage
				has_discrimination_trait = divenori_heritage
				
				#has_discrimination_trait = middle_eastern_heritage
			}
		}   
	}
}

indigenous_oceanic_clothes_trigger = {
	OR = {
		AND = {
			exists = scope:pop
			scope:pop = {
				culture = {
					has_discrimination_trait = indigenous_oceanic_heritage
				}   
			}
		}
		AND = {
			exists = scope:character
			scope:character = {
				culture = {
					has_discrimination_trait = indigenous_oceanic_heritage
				}   
			}
		}
	}
}

circumpolar_clothes_pop_trigger = {
	exists = scope:pop
	scope:pop = {
		OR = {
			culture = cu:inuit
			culture = {
				has_discrimination_trait = siberian_culture_group
			}
		}
		NOT = { is_pop_type = slaves }
	}
}

sami_clothes_pop_trigger = {
	exists = scope:pop
	scope:pop = {
		culture = cu:testorian
	}
}


african_clothes_pop_trigger = {
	exists = scope:pop
	scope:pop = {
		culture = {
			OR = {
				has_discrimination_trait = central_african_culture_group
				has_discrimination_trait = east_african_culture_group
				has_discrimination_trait = west_african_culture_group
				has_discrimination_trait = southern_african_culture_group
				has_discrimination_trait = malagasy_language
			}
		}   
	}
}

native_american_clothes_pop_trigger = {
	exists = scope:pop
	scope:pop = {
		#Anbennar
		culture = {
			has_discrimination_trait = noruinic

			#has_discrimination_trait = indigenous_american_heritage
		}   
	}
}

south_east_asia_clothes_pop_trigger = {
	exists = scope:pop
	scope:pop = {
		culture = {
			has_discrimination_trait = southeast_halessi_heritage

			#has_discrimination_trait = southeast_asian_heritage
		}   
	}
}

south_american_clothes_character_trigger = {
	exists = scope:character
	scope:character = {
		#Anbennar - changed this to some of our south american colonizer equivalents really
		OR = {
			culture = cu:amadian
			culture = cu:ozgar_orc
			culture = cu:dhanaenno
			culture = cu:swampman
			culture = cu:kioha_harpy

			#culture = cu:platinean
			#culture = cu:brazilian
			#culture = cu:south_andean
			#culture = cu:north_andean
			#culture = cu:mexican
			#culture = cu:central_american
			#culture = cu:caribeno
		}
	}
}

south_american_clothes_pop_trigger = {
	exists = scope:pop
	scope:pop = {
		#Anbennar - changed this to some of our south american colonizer equivalents really
		OR = {
			culture = cu:amadian
			culture = cu:ozgar_orc
			culture = cu:dhanaenno
			culture = cu:swampman
			culture = cu:kioha_harpy

			#culture = cu:platinean
			#culture = cu:brazilian
			#culture = cu:south_andean
			#culture = cu:north_andean
			#culture = cu:mexican
			#culture = cu:central_american
			#culture = cu:caribeno
		}
	}
}

integrated_south_american_natives_pop_trigger = { # Represents those South American native groups that had long been integrated into wider South and Central American society.
	exists = scope:pop
	scope:pop = {
		culture = {
			OR = {
				#has_discrimination_trait = native_mesoamerican_culture_group
				#has_discrimination_trait = south_american_group
				has_discrimination_trait = testorian_heritage
			}
			NOT = {
				OR = {
					#this = cu:patagonian
					#this = cu:amazonian
				}
			}
		}
	}
}

cold_clothes_pop_trigger = {
	exists = scope:pop
	scope:pop = {
		OR = {
			#has_pop_culture = inuit
			#has_pop_culture = athabaskan
			#has_pop_culture = siberian
			has_pop_culture = testorian
		}
	}
}

japanese_clothes_pop_trigger = {
	exists = scope:pop
	scope:pop = {
		#Anbennar - just givin command this
		OR = {
			culture = { has_discrimination_trait = command_heritage	} #heritage so the japanese humans can get it too
			#culture = cu:japanese
			#culture = cu:ainu
		}
	}
}


japanese_clothes_trigger = {
	exists = scope:character
	scope:character = {
		#Anbennar - just givin command this
		OR = {
			culture = { has_discrimination_trait = command_heritage }	#heritage so the japanese humans can get it too
			#culture = cu:japanese
			#culture = cu:ainu
		}
	}
}

indian_clothes_pop_trigger = {
	exists = scope:pop
	scope:pop = {
		#Anbenanr
		OR = {
			culture = {
				has_discrimination_trait = raheni_heritage
				#has_discrimination_trait = south_asian_heritage
			}
			#religion = rel:sikh
		}
	}
}

slavic_clothes_pop_trigger = {
	scope:pop ?= {
		#Anbennar - added the dostanorians to it
		culture = {
			OR = {
				has_discrimination_trait = dostanorian
				
				#has_discrimination_trait = slavic
				#has_discrimination_trait = east_slavic
				#has_discrimination_trait = west_slavic
				#has_discrimination_trait = jugoslav
			}
			
		}   
	}
}

slavic_clothes_trigger = {
	scope:character ?= {
		culture = {
			OR = {
				has_discrimination_trait = slavic
				has_discrimination_trait = east_slavic
				has_discrimination_trait = west_slavic
				has_discrimination_trait = jugoslav
			}
		}   
	}
}


# Anbennar - changed to gerudian
nordic_clothes_pop_trigger = {
	scope:pop ?= {
		culture = {
			#has_discrimination_trait = scandinavian_culture_group	
			has_discrimination_trait = gerudian		
		}   
	}
}

poor_clothes_pop_trigger = {
	exists = scope:pop
     scope:pop = {
       	OR = {
			is_pop_type = farmers
			is_pop_type = peasants
			is_pop_type = laborers
			is_pop_type = machinists
       	}
     }   
}

middle_clothes_pop_trigger = {
     exists = scope:pop
     scope:pop = {
       	OR = {
			is_pop_type = clerks
			is_pop_type = engineers
			is_pop_type = bureaucrats
			is_pop_type = shopkeepers
       	}
     }  
}

upper_clothes_pop_trigger = {
     exists = scope:pop
     scope:pop = {
     	OR = {
			is_pop_type = aristocrats
			is_pop_type = capitalists
			is_pop_type = academics
     	}
     }  
}



soldier_clothes_pop_trigger = {
     exists = scope:pop
     scope:pop = {
		is_pop_type = soldiers
     }
}

officers_clothes_pop_trigger = {
	exists = scope:pop
	scope:pop = {
		is_pop_type = officers
	} 
}

clergy_clothes_pop_trigger = {
	exists = scope:pop
     scope:pop = {
		is_pop_type = clergymen
     }
}

shopkeepers_clothes_pop_trigger = {
     exists = scope:pop
     scope:pop = {
		is_pop_type = shopkeepers	
     }    
}

clerks_clothes_pop_trigger = {
     exists = scope:pop
     scope:pop = {
		is_pop_type = clerks	
     }   
}

academics_clothes_pop_trigger = {
     exists = scope:pop
     scope:pop = {
		is_pop_type = academics	
     }   
}

farmers_clothes_pop_trigger = {
     exists = scope:pop
     scope:pop = {
		is_pop_type = farmers	
     }   
}

laborers_clothes_pop_trigger = {
     exists = scope:pop
     scope:pop = {
		is_pop_type = laborers	
     }   
}

peasants_clothes_pop_trigger = {
     exists = scope:pop
     scope:pop = {
		is_pop_type = peasants	
     }   
}

slaves_clothes_pop_trigger = {
     exists = scope:pop
     scope:pop = {
		is_pop_type = slaves	
     }   
}

aristocrats_clothes_pop_trigger = {
     exists = scope:pop
     scope:pop = {
		is_pop_type = aristocrats	
     }   
}

machinists_clothes_pop_trigger = {
     exists = scope:pop
     scope:pop = {
		is_pop_type = machinists	
     }   
}

engineers_clothes_pop_trigger = {
     exists = scope:pop
     scope:pop = {
		is_pop_type = engineers	
     }   
}

bureaucrats_clothes_pop_trigger = {
     exists = scope:pop
     scope:pop = {
		is_pop_type = bureaucrats	
     }   
}

christian_clothes_pop_trigger = {
     exists = scope:pop
     scope:pop = {
		#Anbennar
		religion = rel:ravelian
		#religion = {
			#has_discrimination_trait = christian
		#}
	}
}

buddhist_clothes_pop_trigger = {
     exists = scope:pop
     scope:pop = {
		#Anbennar
  		OR = {
			religion = {
				has_discrimination_trait = halessi_religion

				#has_discrimination_trait = buddhist
			}
			religion = rel:shinto
		}
     }
}

slightly_underweight_pop_trigger = {
	exists = scope:pop
	scope:pop = {
		OR = {
			AND = {
				pop_weight_modifier_scale < 25	
				standard_of_living >= 10		
			}
			AND = {
				standard_of_living >= 5
				standard_of_living < 10
				pop_weight_modifier_scale >= 25
				pop_weight_modifier_scale < 50
			}			
			AND = {
				standard_of_living < 5
				pop_weight_modifier_scale >= 50
			}		
		}	
    }
}

underweight_pop_trigger = {
	exists = scope:pop
	scope:pop = {
		OR = {
			AND = {
				pop_weight_modifier_scale < 25
				standard_of_living >= 5
				standard_of_living < 10
			}		
			AND = {
				standard_of_living < 5
				pop_weight_modifier_scale >= 25
				pop_weight_modifier_scale < 50
			}		
		}	
    }
}

very_underweight_pop_trigger = {
	exists = scope:pop
	scope:pop = {
		pop_weight_modifier_scale < 25
		standard_of_living < 5	
    }
}

slightly_overweight_pop_trigger = {
	exists = scope:pop
	scope:pop = {
		OR = {
			AND = {
				standard_of_living >= 15
				standard_of_living < 20
				pop_weight_modifier_scale >= 90	
			}
			AND = {
				standard_of_living >= 20
				standard_of_living < 40
				pop_weight_modifier_scale >= 85	
				pop_weight_modifier_scale < 90
			}
			AND = {
				standard_of_living >= 40
				pop_weight_modifier_scale >= 70
				pop_weight_modifier_scale < 80
			}					
		}	
    }
}

overweight_pop_trigger = {
	exists = scope:pop
	scope:pop = {
		OR = {
			AND = {
				standard_of_living >= 15
				standard_of_living < 20
				pop_weight_modifier_scale >= 95
			}
			AND = {
				standard_of_living >= 20
				standard_of_living < 40
				pop_weight_modifier_scale >= 90
				pop_weight_modifier_scale < 95
			}
			AND = {
				standard_of_living >= 40
				pop_weight_modifier_scale >= 80	
				pop_weight_modifier_scale < 90
			}				
		}	
    }
}

very_overweight_pop_trigger = {
	exists = scope:pop
	scope:pop = {
		OR = {
			AND = {
				standard_of_living >= 20
				standard_of_living < 40
				pop_weight_modifier_scale >= 95
			}
			AND = {
				standard_of_living >= 40
				pop_weight_modifier_scale >= 90
			}				
		}	
    }
}

struggling_pop_trigger = {
	exists = scope:pop
	scope:pop = {
		standard_of_living < 10
	}
}

agitator_character_trigger = {
	scope:character ?= {
		has_role = agitator
	}
}

should_be_naked_trigger = {
	exists = scope:pop
	scope:pop = {
       	OR = {
			is_pop_type = farmers
			is_pop_type = peasants
			is_pop_type = laborers
			is_pop_type = slaves
		}
		OR = {
			#culture = cu:amazonian
			#culture = cu:nuba
			#culture = cu:dinka
			culture = cu:testorian
		}
	}
}

## Can only be checked in the interface since the setting isn't synced
should_be_fully_naked_portrait_trigger = {
	should_be_naked_trigger = yes
	should_show_nudity = yes
}
#
#should_be_topless_portrait_trigger = {
#	exists = scope:pop
#	has_character_flag = single_combat_stripped_to_waist
#	should_show_nudity = yes
#}