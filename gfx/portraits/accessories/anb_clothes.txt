﻿# These assets are then used in the genes (game/common/genes/..) at the bottom of the file. 

#############################################################
#															#
# 			 Male Clothes and Clothing Accessories			#
#															#
#############################################################

########################
#					   #									
# 	  	  COATS  	   #	
#					   #									
########################


## CANNORIAN ##


male_coat_dostanor_black_03 = {
	set_tags = "shrink_arms,shrink_chest,shrink_belly"
	entity = { required_tags = ""					shared_pose_entity = torso		entity = "male_coat_dostanor_black_03_entity" }	
}

male_coat_dostanor_black_06 = {
	set_tags = "shrink_arms,shrink_chest,shrink_belly"
	entity = { required_tags = ""					shared_pose_entity = torso		entity = "male_coat_dostanor_black_06_entity" }	
}

male_coat_dostanor_black_10 = {
	set_tags = "shrink_arms,shrink_chest,shrink_belly"
	entity = { required_tags = ""					shared_pose_entity = torso		entity = "male_coat_dostanor_black_10_entity" }	
}

########################
#					   #									
# 	    LEGWEAR  	   #	
#					   #									
########################


## CANNORIAN ##

# male_legwear_european_01_dostanor = {
# 	set_tags = "shrink_legs"
# 	entity = { required_tags = ""					shared_pose_entity = torso		entity = "male_legwear_european_01_dostanor_entity" }	
# }

#############
#           #
#   SASHES  #
#           #
#############


male_sash_vivin_blue = {
	entity = { required_tags = ""					shared_pose_entity = torso		entity = "male_sash_vivin_blue_01_entity" }	
}

male_sash_dostanor = {
	entity = { required_tags = ""					shared_pose_entity = torso		entity = "male_sash_dostanor_01_entity" }	
}

########################
#					   #									
# 	    NECKLACES      #	
#					   #									
########################


male_necklace_ravelian = {
	entity = { required_tags = ""					shared_pose_entity = torso		entity = "male_necklace_ravelian_entity" }	
}