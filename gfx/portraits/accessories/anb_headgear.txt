﻿# These assets are then used in the genes (game/common/genes/..) at the bottom of the file. 

#####################################
#									#
# 			 Male Headgear			#
#									#
#####################################

## Military Headgear ##

male_headgear_dostanor_military_02 = {
	set_tags ="cap1"
	entity = { required_tags = ""					shared_pose_entity = head		entity = "male_headgear_dostanor_military_02_entity" }	
}

male_headgear_dostanor_military_03 = {
	set_tags ="cap1" 
	entity = { required_tags = ""					shared_pose_entity = head		entity = "male_headgear_dostanor_military_03_entity" }	
}