﻿STATE_STEEL_BAY = {
    id = 526
    subsistence_building = "building_subsistence_farms"
    provinces = { "x3f0e92" "x46174b" "x2815ec" "x412250" "x901b46" "x513040" "x992011" "x433e80" "x444e40" "x4346c0" "xc42b3a" "x425abf" "xf32e4c" "x7d50b4" "xff3191" "xab563e" "x605efb" "x627f2f" "x786be0" "x9c6d63" "x0f9d89" "x2da13a" "xfd5f81" "x60a6ed" "x70c845" "xe299b8" "x0ee4cf" "x17f5a9" "x8fd6da" "x2bf5fb" "x6bf6a4" "xc4f634" }
    traits = { "state_trait_natural_harbors" }
    city = "x4346C0" #Ricardsport
    port = "xF32E4C" #New Telgeir
    farm = "xE299B8" #Rionfort
    mine = "x425ABF" #Wretchedcliff
    wood = "x444E40" #Bayfield
    arable_land = 98
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_cotton_plantations bg_dye_plantations bg_tobacco_plantations bg_sugar_plantations }
    capped_resources = {
        bg_fishing = 10
        bg_logging = 12
        bg_iron_mining = 24
        bg_coal_mining = 32
    #1 cobalt-zinc deposit
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 4
    }
    naval_exit_id = 3125 #Matni Sea
}

STATE_GREENHILL = {
    id = 527
    subsistence_building = "building_subsistence_farms"
    provinces = { "x5402a3" "x261b47" "x093195" "x0154a8" "x414e54" "x534c6c" "x4c7d74" "x0f86ec" "x707ee9" "x789118" "xe66d55" "x7d8e7e" "x2aa5f6" "x819c8c" "xb186ee" "xa398dc" "xe58bb3" "x45cd32" "xa4a4ed" "x051f32" "xcbb74b" "xadc571" "x96d701" "x15f0da" "x4bf04a" "x29f7bb" "x4ffe70" "xffd800" "xa4e3fe" "xa9f85a" }
    traits = {}
    city = "x4BF04A" #Tristanton
    port = "x45CD32" #Ruinharbour
    farm = "x051F32" #Varland
    mine = "xA9F85A" #Naraine
    wood = "xFFD800" #Erlanswood
    arable_land = 42
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_cotton_plantations bg_dye_plantations bg_tobacco_plantations bg_sugar_plantations }
    capped_resources = {
        bg_fishing = 10
        bg_logging = 14
        bg_iron_mining = 24
        bg_coal_mining = 10
    #2 cobalt-zinc deposits
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 4
    }
    naval_exit_id = 3124 #Lovers Pass
}

STATE_RYAIL = {
    id = 528
    subsistence_building = "building_subsistence_farms"
    provinces = { "x952121" "xf508eb" "xd98345" "x9e9a42" "xe27ecd" "x5ab6b0" "x91cd55" "xd4e2cf" }
    traits = {}
    city = "x9E9A42" #Random
    port = "x952121" #Random
    farm = "xF508EB" #Random
    wood = "x5AB6B0" #Random
    arable_land = 18
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_tobacco_plantations bg_sugar_plantations }
    capped_resources = {
        bg_fishing = 11
        bg_logging = 5
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 2
    }
    naval_exit_id = 3125 #Matni Sea
}

STATE_SCATTERED_ISLANDS = {
    id = 529
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0024FF" "x15BB4F" "x2D0E85" "x402040" "x538E1F" "x8DFDBD" "x9E3E3E" "xE4547A" "xEF0BBF" "xF9F7D6" }
    traits = {}
    city = "x402040" #Random
    port = "x0024FF" #Random
    farm = "x2d0e85" #Random
    wood = "x8dfdbd" #Random
    arable_land = 10
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_sugar_plantations }
    capped_resources = {
        bg_fishing = 4
        bg_whaling = 2
    }
    naval_exit_id = 3125 #Matni Sea
}

STATE_BRISTAILEAN = {
    id = 531
    subsistence_building = "building_subsistence_farms"
    provinces = { "x039447" "x0D4483" "x2BB631" "x409145" "x412EB0" "x4134E0" "x4FF0CD" "x8052D4" "x993939" "xAC4BA1" }
    traits = {}
    city = "x4ff0cd" #Random
    port = "x993939" #Random
    farm = "x2bb631" #Random
    wood = "x8052d4" #Random
    arable_land = 1
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_tobacco_plantations bg_sugar_plantations }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
    }
    naval_exit_id = 3116 #Torn Sea
}

STATE_GUARDIAN_ISLANDS = {
    id = 532
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0B902B" "x20610D" "x233FBF" "x303ED0" "x3675E0" "x435470" "xe9d85c" "x4B7937" "x562E85" "x583D16" "x59F52B" "x5AD1CB" "x606EBF" "x612C60" "x68A4C9" "x77CE38" "x797F04" "x79CAD0" "x7EF6ED" "x836423" "x873B3E" "x89DF1A" "x8C099A" "x902525" "x94775A" "x959BC9" "xA19252" "x303ed0" "xAB4F13" "xB9630A" "xBB5181" "xC1B876" "xC26FFC" "xCE1A2B" "xD4E2D0" "xDAD8E2" "xDF69C5" "xF56177" }
    traits = {}
    city = "x59f52b" #Random
    port = "x583d16" #Random
    farm = "x902525" #Random
    mine = "xe9d85c" #New Place
    wood = "x836423" #Random
    arable_land = 1
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_cotton_plantations bg_dye_plantations bg_tobacco_plantations bg_sugar_plantations }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
        bg_iron_mining = 9
        bg_lead_mining = 8
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 2
    }
    naval_exit_id = 3116 #Torn Sea
}

STATE_BENTER = {
    id = 533
    subsistence_building = "building_subsistence_farms"
    provinces = { "x18D607" "x35D7D9" "x387FCF" "x4389E1" "x48E7BF" "x749177" "x925151" "xA151C9" "xCADAFF" "xD43535" "xE7ABE7" }
    traits = {}
    city = "x749177" #Random
    port = "xe7abe7" #Random
    farm = "x48e7bf" #Random
    mine = "x387fcf" #New Place
    wood = "xcadaff" #Random
    arable_land = 1
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_cotton_plantations bg_dye_plantations bg_sugar_plantations }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
        bg_iron_mining = 18
        bg_lead_mining = 16
        bg_coal_mining = 1
    #1 bauxite deposit, 1 cobalt-zinc deposit
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 3
    }
    naval_exit_id = 3124 #Lovers Pass
}

STATE_XAYBATENCOS = {
    id = 534
    subsistence_building = "building_subsistence_farms"
    provinces = { "x3E101E" "x48E1C0" "xBB9AD4" "xE755AB" "xEABD5D" }
    traits = {}
    city = "x3e101e" #Random
    port = "x48E1C0" #Random
    farm = "xeabd5d" #Random
    mine = "xe755ab" #Topetl
    wood = "xbb9ad4" #Random
    arable_land = 1
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_cotton_plantations bg_dye_plantations bg_sugar_plantations }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
        bg_iron_mining = 18
        bg_lead_mining = 24
        bg_coal_mining = 1
    }
    resource = {
        type = "bg_gold_fields"
        depleted_type = "bg_gold_mining"
        undiscovered_amount = 4
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 3
    }
    naval_exit_id = 3124 #Lovers Pass
}

STATE_TERRACEWOODS = {
    id = 535
    subsistence_building = "building_subsistence_farms"
    provinces = { "x059F5E" "x1E703F" "x3230E6" "x4389DE" "x48E6BF" "x4D869B" "x4E9BBF" "x67767A" "x71A0CD" "x8CF429" "x94888A" "xAEDF7A" "xB0F65B" "xC1CF2C" "xD23535" "xD7BD1A" }
    traits = {}
    city = "x1e703f" #Random
    farm = "x059f5e" #Random
    mine = "x71a0cd" #New Place
    wood = "x3230e6" #Random
    arable_land = 1
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_dye_plantations bg_sugar_plantations }
    capped_resources = {
        bg_logging = 1
        bg_coal_mining = 1
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 5
    }
}

STATE_MAYTE = {
    id = 536
    subsistence_building = "building_subsistence_farms"
    provinces = { "x60E5ED" "x6784E0" "x689EF0" "x6994A0" "x69A250" "x7BB548" "x8E7A4F" "x9AC029" "xD7BF1A" "xE9B493" }
    traits = { state_trait_harafroy }
    city = "x7bb548" #Random
    port = "xd7bf1a" #Random 
    farm = "x69a250" #Random
    mine = "x6784e0" #New place
    wood = "x6994a0" #Random
    arable_land = 1
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_cotton_plantations bg_dye_plantations bg_sugar_plantations }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
        bg_lead_mining = 8
        bg_coal_mining = 1
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 3
    }
    naval_exit_id = 3116 #Torn Sea
}

STATE_TLACHIBAR = {
    id = 537
    subsistence_building = "building_subsistence_farms"
    provinces = { "x4F8499" "x54D127" "x688840" "x754B6E" "xB9072A" "xB92D20" "xBAEF16" "xC37B79" "xDE5E5E" "xE5F901" }
    traits = {}
    city = "xb92d20" #Random
    farm = "xde5e5e" #Random
    wood = "xc37b79" #Random
    arable_land = 1
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_dye_plantations bg_sugar_plantations }
    capped_resources = {
        bg_logging = 1
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 7
    }
}

STATE_TAMETER = {
    id = 538
    subsistence_building = "building_subsistence_farms"
    provinces = { "x4300F0" "x72344D" "x7F647A" "x994148" "xA063E3" "xA4E315" "xCC800D" "xD2AD4C" "xD2F3C2" "xDEAC95" "xE2DA61" }
    traits = {}
    city = "xd2f3c2" #Random
    farm = "xdeac95" #Random
    wood = "xd2ad4c" #Random
    arable_land = 50
    arable_resources = { bg_maize_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
    }
}

STATE_WILIKINAH = {
    id = 539
    subsistence_building = "building_subsistence_orchards"
    provinces = { "x10D98F" "x1E4E7C" "x387E90" "x3A542B" "x44EB96" "x5F65DE" "x646290" "x6698A2" "xA21D84" "xB1AAE7" "xC32B73" "xD6D15C" }
    traits = { state_trait_harafroy }
    city = "xb1aae7" #Random
    farm = "x3a542b" #Random
    mine = "x6698a2" #Random
    wood = "xc32b73" #Random
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
        bg_iron_mining = 9
    }
}

STATE_NEANKINAH = {
    id = 540
    subsistence_building = "building_subsistence_orchards"
    provinces = { "x01B54E" "x1AE532" "x538E1E" "x5D3D51" "x5E029F" "x6356F0" "x645A50" "x6AACA0" "x73D28F" "x7BB87F" "xA56F3F" "xBA596D" "xBA7ECF" "xBE03D9" "xC18584" "xCBE6B2" "xE04ABF" }
    traits = { state_trait_harafroy }
    city = "x645a50" #Random
    farm = "x6356f0" #Random
    mine = "x1ae532" #New Place
    wood = "x5d3d51" #Random
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
        bg_iron_mining = 9
        bg_lead_mining = 8
    #1 cobalt-zinc deposit
    }
}

STATE_LEETIKINAH = {
    id = 541
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x5676F0" "x6797A2" "xA0816F" "xA8D5EA" "xB6D96A" "xDDCE1D" "xE0374B" "xE04A4B" "xE0804B" "xE0B64B" "xE30A42" "xEAB6C3" "xFA33DC" }
    traits = { state_trait_harafe_desert }
    city = "x6797a2" #Random
    farm = "xa8d5ea" #Random
    mine = "xe30a42" #New Place
    wood = "xe0b64b" #Random
    arable_land = 1
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
        bg_lead_mining = 8
    #1 rare earths deposit
    }
}

STATE_MANGROVY_COAST = {
    id = 542
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0010FF" "x0012FF" "x29FDCF" "x44BCE0" "x62E1C9" "x8E6308" "x927129" "x977BF7" "x97AFCD" "x9AC02C" "xC7DF66" "xDBE07C" "xE40E1B" "xEB4799" }
    traits = { state_trait_harafroy }
    city = "x977BF7" #Random
    port = "x97AFCD" #Random
    farm = "xEB4799" #Random
    wood = "xC7DF66" #Random
    arable_land = 1
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_cotton_plantations bg_dye_plantations bg_sugar_plantations }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 6
    }
    naval_exit_id = 3116 #Torn Sea
}

STATE_NAUFKINAH = {
    id = 543
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0BD7B1" "x1A08A5" "x1A0BA7" "x330AFA" "x3A066D" "x4CD582" "x6CCEF0" "x735250" "x7AB881" "x7B2528" "x7BB280" "x902323" "x98C02C" "xA4BD38" "xB3E66C" "xC7269C" "xC8249B" "xCA849F" "xCBB064" "xE60E1B" }
    traits = { state_trait_harafroy }
    city = "x1a08a5" #Random
    farm = "x7ab881" #Random
    mine = "xca849f" #New Place
    wood = "xc8249b" #Random
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
        bg_coal_mining = 1
    }
}

STATE_YEAKOLEO = {
    id = 544
    subsistence_building = "building_subsistence_farms"
    provinces = { "x00C2FF" "x147220" "x63E1C9" "x71FBFB" "x723840" "x7466F0" }
    traits = {}
    city = "x147220" #Random
    farm = "x00c2ff" #Random
    port = "x723840" #Random
    wood = "x63e1c9" #Random
    arable_land = 1
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_cotton_plantations bg_dye_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
    }
    naval_exit_id = 3116 #Torn Sea
}

STATE_ZURZUMEXIA = {
    id = 545
    subsistence_building = "building_subsistence_farms"
    provinces = { "x1AE530" "x208CDD" "x317A7D" "x702040" "x727337" "x848CDD" "xA53899" "xE08CDD" }
    traits = {}
    city = "xe08cdd" #Random
    farm = "x1ae530" #Random
    port = "x848cdd" #Random
    mine = "x727337" #New Place
    wood = "x317a7d" #Random
    arable_land = 1
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_cotton_plantations bg_dye_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
        bg_lead_mining = 8
        bg_coal_mining = 1
    }
    naval_exit_id = 3116 #Torn Sea
}

STATE_SPORK_RALD = {
    id = 546
    subsistence_building = "building_subsistence_farms"
    provinces = { "x51066D" "x6FF4A0" "x6FFEF0" "x8E5086" "xCD5552" "xDB6C09" "xE8826A" }
    traits = {}
    city = "xdb6c09" #Random
    farm = "xcd5552" #Random
    port = "x51066d" #Random
    mine = "xe8826a" #New Place
    wood = "x6ff4a0" #Random
    arable_land = 1
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_cotton_plantations bg_dye_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
        bg_coal_mining = 1
    }
    naval_exit_id = 3120 #Artificers Coast
}

STATE_MESTIKARDU = {
    id = 547
    subsistence_building = "building_subsistence_farms"
    provinces = { "x118A5E" "x523EB0" "x6F3383" "x7A3B01" "x962A2A" "x976D8B" "xAFCB42" "xB326D4" "xCD5652" "xE3DA61" "xED2DB8" "xED994F" }
    traits = {}
    city = "xed994f" #Random
    farm = "xe3da61" #Random
    port = "x118a5e" #Random
    wood = "x976d8b" #Random
    arable_land = 1
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_cotton_plantations bg_dye_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
    }
    naval_exit_id = 3120 #Artificers Coast
}

STATE_RAZPAK = {
    id = 548
    subsistence_building = "building_subsistence_farms"
    provinces = { "x19E532" "x3BFC3E" "x7778C0" "x943750" "x9B87F7" "xB93333" "xBCCC4A" "xD9D8E2" "xE1303F" "xFFA16B" }
    traits = {}
    city = "xd9d8e2" #Random
    farm = "xbccc4a" #Random
    mine = "xe1303f" #New Place
    wood = "x19e532" #Random
    arable_land = 1
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_cotton_plantations bg_dye_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
        bg_coal_mining = 1
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 3
    }
}

STATE_MIKIDAP = {
    id = 549
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0146AD" "x22BB94" "x44BDE0" "x615A03" "x73D9B6" "x778A90" "x7BB880" "x8D7ED7" "x8E6408" "xC4BB15" "xE34832" "xE67571" }
    traits = { state_trait_harafroy }
    city = "xe34832" #Random
    farm = "xc4bb15" #Random
    mine = "x0146ad" #New Place
    wood = "xe67571" #Random
    arable_land = 1
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_dye_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
        bg_lead_mining = 8
        bg_coal_mining = 1
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 5
    }
}

STATE_TSANKINAH = {
    id = 550
    subsistence_building = "building_subsistence_farms"
    provinces = { "x2AE755" "x725250" "x799840" "x7A2628" "x7AAEF0" "x7B2628" "x7B7143" "x808B9A" "x8375B9" "xB11019" "xCDD142" "xDA37AE" }
    traits = { state_trait_harafe_desert }
    city = "x8375b9" #Random
    farm = "x2ae755" #Random
    mine = "xda37ae" #New Place
    wood = "x808b9a" #Random
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
        bg_coal_mining = 1
    }
}

STATE_KABAHNKINAH = {
    id = 551
    subsistence_building = "building_subsistence_farms"
    provinces = { "x051790" "x0DE70B" "x293FD0" "x2AFDCF" "x57E690" "x6015F9" "x6BB4E0" "x767EF0" "x7885BB" "x8F0CEB" "x927372" "xAAB9D4" "xB8588E" "xC6249B" }
    traits = { state_trait_harafe_desert }
    city = "x57e690" #Random
    farm = "xaab9d4" #Random
    port = "x6015F9" #Random
    mine = "x927372" #New Place
    wood = "x293fd0" #Random
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
        bg_iron_mining = 9
    }
    naval_exit_id = 3121
}

STATE_DRABS_CAMBANN = {
    id = 552
    subsistence_building = "building_subsistence_farms"
    provinces = { "x000EC0" "x156DBE" "x17E176" "x518E42" "x6A2231" "x94F0FE" "x9A34B3" "xA3CD40" "xCBAD63" "xEDF132" "xEEFE94" }
    traits = {}
    city = "x518E42" #Random
    farm = "x000EC0" #Random
    port = "x94F0FE" #Random
    wood = "xA3CD40" #Random
    arable_land = 1
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_dye_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 5
    }
    naval_exit_id = 3121
}

STATE_KOORAS = {
    id = 553
    subsistence_building = "building_subsistence_farms"
    provinces = { "x20438A" "x214295" "x415A45" "x456640" "x547339" "x793636" "x7CD290" "xB9C527" "xBD03D9" "xF933DC" }
    traits = { state_trait_harafroy }
    city = "x793636" #Random
    farm = "xB9C527" #Random
    mine = "x214295" #New Place
    wood = "x7CD290" #Random
    arable_land = 1
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_cotton_plantations bg_dye_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
        bg_coal_mining = 1
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 5
    }
}

STATE_GOMMIOCHAND = {
    id = 554
    subsistence_building = "building_subsistence_farms"
    provinces = { "x219B09" "x429CBA" "x545080" "x6BDA6F" "x911818" "xC5E1BF" "xE0D5B2" }
    traits = { state_trait_harafroy }
    city = "x545080" #Random
    farm = "x219B09" #Random
    port = "xC5E1BF" #Random
    wood = "x6BDA6F" #Random
    arable_land = 1
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_cotton_plantations bg_dye_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
    }
    naval_exit_id = 3120 #Artificers Coast
}

STATE_TALDOUD_KAYSA = {
    id = 555
    subsistence_building = "building_subsistence_farms"
    provinces = { "x25FDCF" "x28FDCF" "x29963D" "x38ED3F" "x405A45" "x7C3410" "xA1A5E1" "xBE3535" "xC100F1" "xC7249B" "xCDD141" }
    traits = {}
    city = "xC100F1" #Random
    farm = "x38ED3F" #Random
    port = "xA1A5E1" #Random
    wood = "x7C3410" #Random
    arable_land = 1
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_cotton_plantations bg_dye_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 5
    }
    naval_exit_id = 3120 #Artificers Coast
}

STATE_DESERT_OF_THE_HARAFE = {
    id = 556
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x1FC47B" "x330EB5" "x4C5020" "x633AE3" "xD0E711" "xD6F1A0" "xDAAE25" "xFB0023" }
    impassable = { "x4C5020" "xd6f1a0" "xdaae25" "x1fc47b" }
    traits = { state_trait_harafe_desert }
    city = "x4C5020" #Desert
    farm = "x1FC47B" #NEW PLACE
    mine = "x330EB5" #NEW PLACE
    port = "xd0e711" #Fospont
    arable_land = 1
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 1
        bg_lead_mining = 24
    #5 rare earths deposit, 1 diamonds deposit
    }
    naval_exit_id = 3121
}
