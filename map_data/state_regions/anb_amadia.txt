﻿STATE_BRONRIN = {
    id = 557
    subsistence_building = "building_subsistence_farms"
    provinces = { "x162D2B" "x282A04" "x32A88D" "x14F889" "x400B82" "x448957" "x99665D" "xB55775" "x5165F3" "x2F7AA7" "x53DC07" "x2D1F12" "x606898" "x61C647" "xA43046" "x06E3CC" "xB0B54A" "x0EBE3B" "x99B599" "x70AAC1" "xA3BD12" "x553AD7" "xA83362" "xB765F5" "x8178B6" "x89FF68" "xA69020" "xA65320" "x923BEA" "x52937A" "xA34812" "xAB6C96" "xA02F18" "xABD0C0" "xDC13E0" "xAFB54A" "xB70633" "xBD65DB" "xC0B2AF" "xC84E45" "xF95E5F" "xF7160C" "xFADB35" "xFBA6EA" "xFDE06A" "xFF5C47" }
    traits = { state_trait_munasin_river state_trait_natural_harbors }
    city = "xFF5C47" #New Munasport


    farm = "xABD0C0" #River Fork


    mine = "xB0B54A" #Hill


    wood = "x32A88D" #Random


    port = "x162D2B" #By City


    arable_land = 112
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_banana_plantations bg_cotton_plantations bg_dye_plantations bg_sugar_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 6
        bg_fishing = 5
        bg_whaling = 4
    }
    resource = {
        type = "bg_damestear_fields"
        depleted_type = "bg_damestear_mining"
        undiscovered_amount = 3
    }
    naval_exit_id = 3125
}

STATE_NUR_ISTRALORE = {
    id = 558
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0CF17A" "x2B7AA7" "x3F665D" "x58580C" "x838314" "x853674" "x97BA6E" "xA62A00" "xC26610" "xC26AF7" "xCD528C" "xE22915" "xF9045D" "xC6CEC1" "x729FF8" "x80981C" "x3C2C72" "x63D9AF" "xACC987" "x262040" "x58C64F" "x8CBE6D" "xA77690" "x959346" "x3D50A4" "x8061BA" "xE97D71" "xE5BFC1" "x57AF24" "x4C3DB8" "xAACA7E" "x5BDFBF" "x338C27" "xA2B74F" "xF98E18" "x2FAC88" "x43D02F" "x0E5E83" "xD0DDA2" "x797E0B" "x3F86AD" }
    traits = {}
    city = "x2FAC88" #Coast


    farm = "xE97D71" #Upstream


    mine = "xE5BFC1" #Random


    wood = "xACC987" #Upstreamer


    port = "x3F665D" #Coast


    arable_land = 30
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_banana_plantations bg_cotton_plantations bg_dye_plantations bg_sugar_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 4
        bg_fishing = 6
        bg_damestear_mining = 5
    }
    naval_exit_id = 3126
}

STATE_MUNASIN = {
    id = 559
    subsistence_building = "building_subsistence_farms"
    provinces = { "x217F7D" "x44EB64" "x46467A" "x7A8FAA" "x942F20" "xAEB54A" "xB61928" "xB64976" "xD275E4" "xD7AB6D" "xD88CB7" "xDB0AEE" "xF3EFBF" "xC99DC5" "x9332CD" "xEBB7E1" "x974E8E" "x4B52A6" "xCA2B8B" "xBA26A1" "xEB15C7" "xB3CC99" "x0D4EFA" "x51C657" "x109EB9" "x744A7A" }
    traits = { state_trait_munasin_river }
    city = "xD275E4" #Fork


    farm = "x46467A" #Fork


    mine = "x4B52A6" #Random


    wood = "x4B52A6" #Random


    arable_land = 98
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_banana_plantations bg_cotton_plantations bg_dye_plantations bg_sugar_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 19
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 15
    }
    resource = {
        type = "bg_damestear_fields"
        depleted_type = "bg_damestear_mining"
        undiscovered_amount = 3
    }
}

STATE_SILVEGOR = {
    id = 560
    subsistence_building = "building_subsistence_farms"
    provinces = { "x10CB48" "x160386" "x303EA5" "x35A88D" "x424A7F" "x503DB8" "x56526D" "x5FBC01" "x666898" "xA303C7" "xD11E6A" "xE3D4CE" "xEE7629" "xF008EB" "xFFA03A" "xFFF26D" "xD16A2C" "x7EC66B" "xA127B1" "xFFA389" "x46471B" "xE8DFA6" "x365B48" "x823E69" }
    traits = { state_trait_silver_range }
    city = "xD16A2C" #River


    farm = "x5FBC01" #Random


    mine = "x503DB8" #Range


    wood = "x35A88D" #Random


    port = "x56526D" #River


    arable_land = 86
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_cotton_plantations bg_dye_plantations bg_sugar_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 15
        bg_fishing = 4
        bg_iron_mining = 21
        bg_coal_mining = 20
        bg_lead_mining = 12
        bg_gold_mining = 1
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 14
    }
    resource = {
        type = "bg_gold_fields"
        depleted_type = "bg_gold_mining"
        undiscovered_amount = 4
    }
    resource = {
        type = "bg_damestear_fields"
        depleted_type = "bg_damestear_mining"
        undiscovered_amount = 3
    }
    naval_exit_id = 3125
}

STATE_CLAMGUINN = {
    id = 562
    subsistence_building = "building_subsistence_farms"
    provinces = { "x110619" "x2EF13D" "x3ED117" "x46E73A" "x594678" "x663CFF" "x6B0AF2" "x6B9823" "x998CD6" "xC0BC32" "xCA4298" "xCC6A2C" "xD3DF45" "xD582E1" "xF00DF6" "xF17AB6" "xF379F5" "xFAD495" "x007C3F" "x6F48E6" }
    impassable = { "xF17AB6" }
    traits = { state_trait_munasin_river state_trait_lake_chakantu }
    city = "xD582E1" #Lake


    farm = "x6B9823" #River


    mine = "x007C3F" #Random


    wood = "xC0BC32" #Random


    arable_land = 54
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_banana_plantations bg_dye_plantations bg_sugar_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 13
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 10
    }
    resource = {
        type = "bg_damestear_fields"
        depleted_type = "bg_damestear_mining"
        undiscovered_amount = 3
    }
}

STATE_URANCESTIR = {
    id = 563
    subsistence_building = "building_subsistence_farms"
    provinces = { "x001F32" "x032A4B" "x0FB456" "x33B619" "x37598B" "x3B5D31" "x3CC758" "x430724" "x539040" "x5F363C" "x5F6898" "x678FFD" "x6B9D40" "x6C4EED" "x83C24F" "x866F94" "x86E4D7" "x876272" "x94995A" "xA59E56" "xB4F67F" "xC330F4" "xC6B190" "xD32C3F" "xFBF6AE" "xFCBADF" "xFDD934" "xFF759A" "xC9D3D1" "xD2DC54" "xEF35F7" "x8DFF6B" "x22D884" "x18C0FB" "x41FDB1" }
    traits = { state_trait_silver_range state_trait_lake_chakantu }
    city = "xC330F4" #East Lake


    farm = "x3CC758" #River


    mine = "x37598B" #Hills


    wood = "xEF35F7" #Interior


    arable_land = 56
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_dye_plantations bg_sugar_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 12
        bg_iron_mining = 18
        bg_coal_mining = 32
        bg_sulfur_mining = 72
        bg_gold_mining = 5
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 6
    }
    resource = {
        type = "bg_gold_fields"
        depleted_type = "bg_gold_mining"
        undiscovered_amount = 11
    }
    resource = {
        type = "bg_damestear_fields"
        depleted_type = "bg_damestear_mining"
        undiscovered_amount = 3
    }
}

STATE_CALILVAR = {
    id = 564
    subsistence_building = "building_subsistence_farms"
    provinces = { "x09502B" "x1664D1" "x1857F0" "x3BF894" "x3E17FA" "x5A4055" "xAD6820" "xE42B2B" "xF8A1D6" "x937987" "xAB9E56" "xB87F3E" "x9929A3" "xCCE2BD" "xABAF91" "xB6185A" "xD09E5B" "x61AABA" "x37122F" "xA0ADEE" "x424B78" "xFDFFA7" }
    traits = { state_trait_harenaine_river }
    city = "x61AABA" #Fork


    farm = "xA0ADEE" #Rivers


    mine = "xF8A1D6" #Random


    wood = "x1664D1" #Edge


    arable_land = 49
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 7
        bg_damestear_mining = 2
    }
}

STATE_CYMBEAHN = {
    id = 565
    subsistence_building = "building_subsistence_farms"
    provinces = { "x178381" "x1BC9E9" "x1CCC0E" "x21E11D" "x222B3A" "x224A3A" "x225059" "x44E8D5" "x481C4F" "x48824F" "x4F73B5" "x54673E" "x5BD70C" "x650DBD" "x656898" "x68408D" "x7BD082" "x7D5581" "x8A57FA" "x8A6386" "x901F91" "x95D270" "xA04782" "xA2DD96" "xAD4220" "xB9FF80" "xBFA872" "xD4D852" "xDB6609" "xE9DB67" "xF9B1B3" "x83DDF5" "xBEFF80" "xED8347" "x8B64A4" "x1D410B" "x9DF327" "x2DF51C" "x271FF6" }
    traits = { state_trait_harenaine_river }
    city = "x95D270" #River


    farm = "xED8347" #Fork


    mine = "x48824F" #Random


    wood = "x7BD082" #Random


    port = "x225059" #River


    arable_land = 44
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 4
        bg_fishing = 4
    }
    resource = {
        type = "bg_damestear_fields"
        depleted_type = "bg_damestear_mining"
        undiscovered_amount = 3
    }
    naval_exit_id = 3126
}

STATE_OOMU_NELIR = {
    id = 566
    subsistence_building = "building_subsistence_farms"
    provinces = { "x17823D" "x1CE420" "x21A23E" "x2C8E55" "x442871" "x5B9252" "x5C8073" "x6215B4" "x7741DF" "x8E921D" "x966630" "x9C190D" "x9FF9E3" "xA8AF12" "xAC3F03" "xD0729D" "xE354CE" "xE5F393" "xE7AEC6" "xE864BD" "xE96601" "x815BE3" "x490DF3" "x90AEEB" "x25F21F" "x114FAA" "xAAE3DA" "x7F1F0C" "xAF8DD1" }
    traits = { state_trait_silver_range state_trait_lake_chakantu }
    city = "x17823D" #Lake


    farm = "x490DF3" #Lake


    mine = "x9FF9E3" #Silver


    wood = "x21A23E" #Lake


    arable_land = 63
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_sugar_plantations }
    capped_resources = {
        bg_logging = 10
        bg_coal_mining = 36
        bg_sulfur_mining = 48
        bg_gold_mining = 8 #Potosi


        bg_damestear_mining = 8
    }
    resource = {
        type = "bg_gold_fields"
        depleted_type = "bg_gold_mining"
        undiscovered_amount = 15
    }
    resource = {
        type = "bg_damestear_fields"
        depleted_type = "bg_damestear_mining"
        undiscovered_amount = 3
    }
}

STATE_CARA_LAFQUEN = {
    id = 567
    subsistence_building = "building_subsistence_farms"
    provinces = { "x129071" "x16823D" "x1C606B" "x2026FF" "x2A7F7F" "x474080" "x5B7000" "x82D7C0" "x9283AE" "x9438A9" "xA6F548" "xB2FE80" "xB4994F" "xC0D845" "xC53170" "xC6C49F" "xCFE977" "xD153B1" "xD97564" "xEF62C6" }
    traits = { state_trait_harenaine_river state_trait_good_soils state_trait_lake_chakantu }
    city = "x2026FF" #CL


    farm = "xEF62C6" #Rivers


    mine = "x2A7F7F" #Hill


    wood = "xC0D845" #Lake


    arable_land = 58
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 8
        bg_coal_mining = 16
        bg_lead_mining = 16
    }
    resource = {
        type = "bg_damestear_fields"
        depleted_type = "bg_damestear_mining"
        undiscovered_amount = 3
    }
}

STATE_HARENAINE = {
    id = 568
    subsistence_building = "building_subsistence_farms"
    provinces = { "x06A0C3" "x0B0034" "x131280" "x1C1B13" "x2ED02B" "x3620F1" "x3E5B76" "x4C831A" "x576BF1" "x63645A" "x660C91" "x741113" "x74BC71" "x7C4355" "x7FA589" "x8A5B98" "x930C1A" "xA1D632" "xA51FC4" "xA69E56" "xBA83F6" "xC391AD" "xC757F1" "xCD0DA5" "xCD7454" "xD10C7C" "xD71FE1" "xE0D5EA" "xED82DA" "xF7FD87" "x5564AA" "x961D76" "xC79E7C" "x43D888" "xF4FF2E" "x8AFDBE" "x02FE8F" "x4DC73D" "xAFA856" "xEB1ECC" "xF6B1B3" "x34F76A" "x1DCBA3" "xE4E75F" "xC9CA1B" "xAB0D35" "xB1FF00" "xB1AE4F" }
    traits = { state_trait_harenaine_river state_trait_silharenaine_desert }
    city = "xA69E56" #Fork


    farm = "x2ED02B" #River


    mine = "x74BC71" #Random


    arable_land = 22
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_sulfur_mining = 20
    }
    resource = {
        type = "bg_gold_fields"
        depleted_type = "bg_gold_mining"
        undiscovered_amount = 4
    }
    resource = {
        type = "bg_damestear_fields"
        depleted_type = "bg_damestear_mining"
        undiscovered_amount = 3
    }
}

STATE_ARANLAS = {
    id = 569
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0042D9" "x04B62E" "x04C938" "x0AD1B0" "x0B302F" "x2CBC9D" "x37BCFE" "x3CA12E" "x406FF0" "x4F2EAE" "x4FC73D" "x7032F3" "x7B33C4" "x80017C" "x8DAB51" "x9A0C1A" "xA1D9E3" "xA796AA" "xAFA555" "xB3724F" "xB4F2DF" "xB783F6" "xBE4087" "xBF9DA3" "xCA324F" "xD2F73D" "xD484B1" "xF6E162" "xF91328" "x32F5EE" "x094E6B" "xF9F991" "xA27D75" "xDECBD6" "x63336D" "x616898" "x1789CB" "xBC528F" "x994D97" }
    traits = { state_trait_harenaine_river state_trait_silharenaine_desert state_trait_shimmering_mountain }
    city = "xA796AA" #Damestear


    farm = "xBF9DA3" #River


    mine = "x37BCFE" #Damestear


    wood = "xB4F2DF" #Jungle Edge


    arable_land = 20
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_logging = 2
        bg_iron_mining = 33
        bg_lead_mining = 15
        bg_sulfur_mining = 24
        bg_damestear_mining = 20 #Shimmering Mountain


    }
    resource = {
        type = "bg_damestear_fields"
        depleted_type = "bg_damestear_mining"
        undiscovered_amount = 10
    }
}
